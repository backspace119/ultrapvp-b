package com.me.backspace119.enums;

public enum Badge {
	// probably shouldn't use an Enum for this because of inability to make
	// custom badges...
	FIRST_KILL("Get your first kill in UltraPvP", "Murderer", 1, 25, 1),
	TEN_KILLS("Get ten kills in UltraPvP", "Serial Killer", 2, 50, 2),
	HUNDRED_KILLS("Get 100 kills in UltraPvP", "Sociopath", 3, 500, 5),
	HEAD_HUNTER("Collect someone's head.", "Head Hunter", 2, 100, 5);
	final String description;
	final String name;
	final int prestige;
	final int xpReward;
	final int killCoinReward;

	Badge(String description, String name, int prestige, int xpReward, int killCoinReward) {
		this.description = description;
		this.name = name;
		this.prestige = prestige;
		this.xpReward = xpReward;
		this.killCoinReward = killCoinReward;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public int getPrestige() {
		return prestige;
	}

	public int getXPReward() {
		return xpReward;
	}

	public int getKillCoinReward() {
		return killCoinReward;
	}
}

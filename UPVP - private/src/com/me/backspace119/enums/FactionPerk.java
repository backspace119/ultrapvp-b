package com.me.backspace119.enums;

public enum FactionPerk {

	MURDER(
			"Kills the given players name or a random player on the other team if no name is given. Cooldown of 500 seconds - 2 seconds per level",
			"", 500, 10, 100, 2),
	INVINCIBILITY(
			"Gives your faction spawn protection (invincibility) for 10 seconds. Cooldown of 400 seconds - 2 seconds per level.",
			"", 400, 15, 50, 2),
	NUKE(
			"Sends you an 8 digit code that you must repeat within 5 seconds. If successful, you win the game and get a kill for each player on the other team. If unsuccessful, your entire team will be killed. cannot be used until your team has at least 40 kills. Does not cool down.",
			"", -1, 25, 1, 0),
	BOMB(
			"Kills all players on the other team. Cooldown of 1000 seconds - 1 second per level. Cannot be used until you have 20 kills.",
			"", 1000, 20, 100, 1);

	final String description;
	final int coolDown;
	final int levelRequired;
	final int maxLevel;
	final int multiplier;
	final String faceName;

	FactionPerk(String description, String faceName, int coolDown, int levelRequired, int maxLevel, int multiplier) {
		this.description = description;
		this.coolDown = coolDown;
		this.levelRequired = levelRequired;
		this.maxLevel = maxLevel;
		this.multiplier = multiplier;
		this.faceName = faceName;
	}

	public String getFaceName() {
		return faceName;
	}

	public String description() {
		return description;
	}

	public int getCoolDown() {
		return coolDown;
	}

	public int getLevelRequired() {
		return levelRequired;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public int getMultiplier() {
		return multiplier;
	}

}

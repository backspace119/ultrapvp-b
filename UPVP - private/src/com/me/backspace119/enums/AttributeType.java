package com.me.backspace119.enums;

public enum AttributeType {
	SWORD_DAMAGE_MULTIPLIER("SwordDamageMultiplier",
			"Multiplies all damage dealt with a sword by this amount"),
	SWORD_DAMAGE_BONUS("SwordDamageBonus",
			"Adds this amount to all damage dealt with a sword."),
	BOW_DAMAGE_MULTIPLIER("BowDamageMultiplier",
			"Multiplies all damage dealt with a bow by this amount."),
	BOW_DAMAGE_BONUS("BowDamageBonus",
			"Adds this amount to all damage dealt with a bow"),
	DAMAGE_MULTIPLIER("DamageMultiplier", "Self explanatory"),
	DAMAGE_BONUS("DamageBonus", "Self explanatory"),
	HAS_AURA(
			"HasAura",
			"Value of yes or no. This allows players to access the 'Tank' command whilst in a game throwing up an inpenatrable aura for 10 seconds"),
	HAS_CHANGE(
			"HasChange",
			"Value of yes or no. This allows players to access the 'Disguise' command whilst in game allowing them to appear with the other teams Armor as if they were on that team."),
	DAMAGE_TAKE_SWORD_MULT("SwordDamageTakeMult",
			"Multiplier for how much extra damage this class takes from swords"),
	DAMAGE_TAKE_BOW_MULT("BowDamageTakeMult",
			"Multiplier for how much extra damage this class takes from bows"),
	DAMAGE_TAKE_MULT("DamageTakeMult",
			"Multiplier for how much extra damage this class takes period"),
	JUMP_BOOST("JumpBoost",
			"Level of jump boost to apply to the player (0 for none)");

	private final String description;
	private final String name;

	AttributeType(String name, String description) {
		this.description = description;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String description() {
		return description;
	}
}

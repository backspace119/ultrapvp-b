package com.me.backspace119.enums;

public enum GameTypes {
	// in process of moving away from this
	TEAM_PVP("TeamPvP", true),
	FREE_FOR_ALL("FFA", false),
	CTF("CTF", true),
	TEAM_ZOMBIES("TeamZombies", true),
	COOP_ZOMBIES("CoopZombies", false),
	CONTROLPOINT("ControlPoint", true),
	FACTIONMATCH("FactionMatch", true),
	AI("AI", false);

	private final String name;
	private final boolean isTeam;

	GameTypes(String name, boolean isTeam) {
		this.name = name;
		this.isTeam = isTeam;
	}

	public boolean isTeam() {
		return isTeam;
	}

	public String getName() {
		return name;
	}
}

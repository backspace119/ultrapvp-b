package com.me.backspace119.enums;

public enum Perk {
	SECOND_WIND(
			"Gives you a second chance after the first time you're killed. Cooldown of 180 seconds - 1 second per perklevel. Requires level 5. Max perklevel 60.",
			180, 1, "SecondWind", 60, 5),
	KEEP_ITEMS(
			"Lets you keep your items you have bought or otherwise aquired when you die. Cooldown 60 seconds - 1 second per perklevel. Requires perklevel 1. Max Level 45",
			60, 1, "KeepItems", 45, 1),
	INSTA_KILL(
			"Lets your next hit insta-kill its target. Cooldown of 600 seconds - 1 second per perklevel. Requires level 14. Max perklevel 120.",
			600, 1, "Insta-Kill", 120, 14),
	LONGER_SPAWNPROTECT(
			"Makes your respawn protection last 5 seconds longer. Cooldown of 30 seconds - 1 second per perklevel. Requires level 3. Max perkLevel 30.",
			60, 1, "LongerSpawnProtect", 30, 3),
	DOUBLE_COINS(
			"Gives you double the kill coins you would normally get for a kill. Cooldown of 20 seconds - 1 second per perklevel. Requires level 5. Max perklevel 10.",
			20, 1, "DoubleCoins", 10, 5),
	DOUBLE_XP(
			"Gives you double xp for kills you get in a game. No cooldown no leveling. Requires level 5.",
			0, 0, "DoubleXP", 1, 5),
	SHARE_PAIN(
			"Makes anyone who kills you take the damage they dealt to you in the killing blow. Cooldown of 60 seconds - 1 second per perklevel. Requires level 10. Max level 30.",
			60, 1, "SharethePain", 30, 10),
	HEADS_UP(
			"Allows you to take the head of your enemy as a souvenir after the game is over. Cooldown of 600 seconds - 2 seconds per level. Requires level 10. Max level 100",
			600, 2, "HeadsUp", 100, 10);

	private final String description;
	private final int coolDown;
	private final int modifier;
	private final String faceName;
	private final int maxLevel;
	private final int levelRequired;

	Perk(String description, int coolDown, int modifier, String faceName, int maxLevel, int levelRequired) {
		this.description = description;
		this.coolDown = coolDown;
		this.modifier = modifier;
		this.faceName = faceName;
		this.maxLevel = maxLevel;
		this.levelRequired = levelRequired;
	}

	public String description() {
		return description;
	}

	public int getCooldown() {
		return coolDown;
	}

	public int getModifier() {
		return modifier;
	}

	public String getFaceName() {
		return faceName;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public int getLevelRequired() {
		return levelRequired;
	}
}

package com.me.backspace119.games;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.player.PerksHandler;

public class ControlPointGame extends RedBlueTeamGame {
	List<Block> redPoints, bluePoints, redControls, blueControls;

	public ControlPointGame(String name, JavaPlugin plugin, Location redl, Location bluel, List<Location> spectatorLocations, int winKills, List<Block> chests, int ticksInvincible, ItemStack[] redArmor, ItemStack[] blueArmor, GameTypes type, List<Block> refrencedBlocks_1, List<Block> refrencedBlocks_2, World world, boolean hasWorld, String worldName) {
		super(name, plugin, redl, bluel, spectatorLocations, winKills, chests, ticksInvincible, redArmor, blueArmor, type, world, hasWorld, worldName);
		setup(refrencedBlocks_1, refrencedBlocks_2);

	}

	/**
	 * it is standard procedure to override this method and add any additional
	 * game info of your game type to a message to the player.
	 */
	@Override
	public boolean joinGame(Player player, String prefrence) {
		boolean r = super.joinGame(player, prefrence);
		player.sendMessage(ChatColor.GREEN + "Break the other teams colored wool control points to capture them. You can recapture points if they are taken!");
		return r;
	}

	public void addCapture(Player player) {
		coinMap.put(player, coinMap.get(player) + 1);
		if (PerksHandler.hasPerk(player, Perk.DOUBLE_COINS)) {
			coinMap.put(player, coinMap.get(player) + 1);
			PerksHandler.resetPerk(player, Perk.DOUBLE_COINS);
		}
		updateCoinBoard(player.getUniqueId());
		addPoint(player.getUniqueId());
		if (getRedPlayers().contains(player)) {

			red.setTeamScore(red.getTeamScore() + 25);
			addPoint(player.getUniqueId());

		} else {
			blue.setTeamScore(blue.getTeamScore() + 25);
			addPoint(player.getUniqueId());
		}
	}

	public List<Block> getRedPoints() {
		return redPoints;
	}

	public List<Block> getBluePoints() {
		return bluePoints;
	}

	public boolean checkWin() {
		return (blueControls.size() < 1 || redControls.size() < 1);
	}

	public List<Block> getBlueControls() {
		return blueControls;
	}

	public List<Block> getRedControls() {
		return redControls;
	}

	public void setup(List<Block> r1, List<Block> r2) {

		respawnTime = 5;
		redPoints = r1;
		bluePoints = r2;
		if (getRedControls() != null) {
			getRedControls().clear();
			getBlueControls().clear();
		}else{
			redControls = new ArrayList<Block>();
			blueControls = new ArrayList<Block>();
		}
		if (r1 != null) {
			for (Block b : r1) {
				getRedControls().add(b);
				Utils.getChestList().add(b.getLocation());
			}
			for (Block b : r2) {
				getBlueControls().add(b);
				// b.setType(Material.WOOL);
				Utils.getChestList().add(b.getLocation());
			}
		} else {
			System.out.println("null points for map: " + name);
		}

		respawnTime = 0;
	}

	@Override
	public void restart() {
		
		super.restart();
		redPoints.clear();
		bluePoints.clear();
		redPoints = Utils.resetControlPointSet(name + ".r1", name);
		bluePoints = Utils.resetControlPointSet(name + ".r2", name);
		setup(redPoints, bluePoints);
		
	}
}

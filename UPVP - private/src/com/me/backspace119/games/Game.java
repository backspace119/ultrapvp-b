package com.me.backspace119.games;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.player.PerksHandler;
import com.me.backspace119.utils.handlers.player.PlayerGearHandler;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;
import com.onarandombox.MultiverseCore.MultiverseCore;

/**
 * ALL CODE IN THIS PROGRAM IS OPEN SOURCE AND WILL AT SOME POINT BE FREELY
 * AVAILABLE!!! ALL CODE IS UNDER THE GPL v. 4 LICENSE MEANING IF YOU USE /ANY/
 * OF MY CODE YOUR CODE MUST BE OPEN SOURCE AND UNDER THE GPL v. 4 LICENSE AS
 * WELL!!! FULL DOCUMENTATION FOR THE LICENSE WILL APPEAR HERE AND ON MY GIT
 * SOON!!!!!! DO NOT UNDER ANY CIRCUMSTANCE /STEAL/ MY CODE AND THEN PROCEED TO
 * PUT IT IN A PROPRIETARY OR CLOSED SOURCE PROGRAM!!! LEGAL ACTION CAN AND WILL
 * ENSUE!
 * 
 * This is the BASE game class. It is NOT for specific games. (well it has some
 * specifics in it but that is being moved away from at present moment) It has
 * basic functionality that all games need i.e. adding players to the game,
 * keeping track of the scoreboard, resetting the map after the game is done,
 * handling when to finish the game, teleporting players here and there,
 * enforcing /some/ of the game rules, informing players of various things in
 * game, etc. etc. Use this to access low level game functionality. If you need
 * more specialized functionality please access one of the subclasses of this
 * 
 * If you are coding a plugin that makes a new GameType for this plugin then the
 * best place to start would be by extending this class.
 * 
 * @author backspace119
 * 
 */
public class Game {
	protected boolean hasWorld;
	protected List<UUID> invincible = new ArrayList<UUID>();
	protected int ticksInvincible;
	protected boolean restarting;
	protected Environment environment;
	private Map<Player, Objective> objectiveMap = new HashMap<Player, Objective>();
	protected Map<Player, Integer> coinMap = new HashMap<Player, Integer>();

	// to whomever it may concern: putting players in a list like so is ok here
	// they never change worlds and will be held captive within a map until the
	// game is over
	protected List<Player> players = new ArrayList<Player>();
	protected JavaPlugin plugin;
	protected List<Integer> runningTasks;
	protected ItemStack[] inventory;
	protected List<Location> spawnLocations;
	private List<Player> chatList = new ArrayList<Player>();
	protected List<UUID> kickedPlayers = new ArrayList<UUID>();
	protected int winKills;
	protected List<Chest> chests = new ArrayList<Chest>();
	protected Map<UUID, Score> playerScoreMap = new HashMap<UUID, Score>();
	protected Map<UUID, Score> killCoinMap = new HashMap<UUID, Score>();
	protected List<Player> spectators = new ArrayList<Player>();
	protected Scoreboard board;
	protected String name;

	protected List<Location> spectatorLocations = new ArrayList<Location>();
	protected int respawnTime;
	protected GameTypes type;
	protected ScoreboardManager manager = Bukkit.getScoreboardManager();
	protected List<UUID> disguised;
	protected List<UUID> tanking;
	protected World world;
	protected String worldName;

	public Game(String name, JavaPlugin plugin, List<Location> spectatorLocations, int winKills, List<Block> chests, int ticksInvincible, GameTypes type, World world, boolean hasWorld, String worldName, List<Location> spawnLocations) {
		this.hasWorld = hasWorld;
		this.world = world;
		this.worldName = worldName;
		runningTasks = new ArrayList<Integer>();
		this.type = type;
		this.spectatorLocations = spectatorLocations;
		Utils.getGameMap().put(name, this);
		this.plugin = plugin;
		this.name = name;
		this.environment = world.getEnvironment();
		this.spawnLocations = spawnLocations;
		board = manager.getNewScoreboard();
		Objective objective = board.registerNewObjective("UltraPVP", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		Score score = objective.getScore(ChatColor.GOLD + "Kills to Win:");
		score.setScore(winKills);
		this.ticksInvincible = ticksInvincible;
		this.winKills = winKills;
		disguised = new ArrayList<UUID>();
		tanking = new ArrayList<UUID>();

		if (hasWorld) {
			if (world == null) {
				world = plugin.getServer().createWorld(new WorldCreator(worldName));

				MultiverseCore p = (MultiverseCore) plugin.getServer().getPluginManager().getPlugin("Multiverse-Core");

				//plugin.getServer().createWorld(new WorldCreator(world.getName()));
				p.getCore().getMVWorldManager().loadWorld(world.getName());
				//world = plugin.getServer().getWorld(world.getName());

			}
			world.setAutoSave(false);
			checkChests(chests);
		}
	}

	public void checkChests(List<Block> chests) {
		if (chests != null) {
			for (Block chest : chests) {
				if ((chest.getType().equals(Material.CHEST))) {
					Chest c = (Chest) chest.getState();
					c.getInventory().setContents(Utils.getShopChestInventory());
					//System.out.println("[UPVP] good chest for game: " + name + " at location " + c.getX() + " " + c.getY() + " " + c.getZ());
					this.chests.add(c);
					Utils.getChestList().add(c.getLocation());
				}/* else {
					System.out.println("[UPVP] bad chest for game: " + name);
				}*/
			}
		}
	}

	public String getName() {
		return name;
	}

	public List<Chest> getChests() {
		return chests;
	}

	public World getWorld() {
		if (world == null) {
			return plugin.getServer().getWorld("UPVP");
		}
		return world;
	}

	public List<UUID> getDisguised() {
		return disguised;
	}

	public List<Integer> getTaskList() {
		return runningTasks;
	}

	public GameTypes getType() {
		return type;
	}

	public void joinAsSpectator(Player player) {
		chatList.add(player);
		spectators.add(player);
		player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, Integer.MAX_VALUE));
		player.setGameMode(GameMode.CREATIVE);
		player.setWalkSpeed(0F);
		player.setCanPickupItems(false);
		teleportSpectator(player);
	}

	protected Score addScoreboardInfo(Player player, String info, int value) {
		Score score = objectiveMap.get(player).getScore(info);
		score.setScore(value);
		return score;
	}

	// not yet functional will be at a later time when needed
	protected void updateScoreboardInfo() {

	}

	public void teleportSpectator(Player player) {
		player.teleport(spectatorLocations.get(rand.nextInt(spectatorLocations.size())));
	}

	public List<Player> getSpectators() {
		return spectators;
	}

	public void broadcast(final String message) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@Override
			public void run() {
				for (Player p : chatList) {
					p.sendMessage(message);
				}
			}
		}, 1);
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void broadcastAll(final String message) {
		for (Player p : getPlayers()) {
			p.sendMessage(message);
		}
	}

	public List<UUID> getKickedPlayers() {
		return kickedPlayers;
	}

	public Map<Player, Integer> getCoins() {
		return coinMap;
	}

	public void endGame() {
		for (Location l : Utils.getSignsForGame(name)) {

			Sign sign = (Sign) plugin.getServer().getWorld(l.getWorld().getName()).getBlockAt(l).getState();

			sign.setLine(1, ChatColor.RED + "Restarting");

			sign.update();

		}
		getTanking().clear();
		for (int i : runningTasks) {
			plugin.getServer().getScheduler().cancelTask(i);
		}

		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@Override
			public void run() {

				restart();
			}

		}, 20);

	}

	public void addPoint(UUID id) {
		playerScoreMap.get(id).setScore(playerScoreMap.get(id).getScore() + 1);
	}

	public int getScore(UUID id) {
		return playerScoreMap.get(id).getScore();
	}

	public void updateCoinBoard(UUID id) {
		killCoinMap.get(id).setScore(coinMap.get(plugin.getServer().getPlayer(id)));

	}

	public List<UUID> getTanking() {
		return tanking;
	}

	public void playParticlesAtPlayerForPlayer(Player atPlayer, Player forPlayer) {
		forPlayer.playEffect(atPlayer.getLocation(), Effect.MOBSPAWNER_FLAMES, false);
	}

	/**
	 * 
	 * @param player
	 *            player who instantiated join
	 * @param prefrence
	 *            ONLY USED WITHIN SUB CLASSES! LETS YOU ASSIGN THEM TO A
	 *            PARTICULAR TEAM IF IT IS A TEAM GAME! NEEDS TO BE REWORKED!
	 *            NOT NECESSARY IN SUPER CLASS
	 * 
	 * @return technically returns true if the player is teleported
	 *         correctly....this value is seldom used though so it is not that
	 *         important
	 */
	public boolean joinGame(Player player, String prefrence) {
		boolean r = teleportPlayer(player);
		players.add(player);
		Scoreboard playerBoard = manager.getNewScoreboard();

		Objective objective = playerBoard.registerNewObjective("UltraPVP", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		Score score = objective.getScore(ChatColor.GOLD + "Kills to Win:");
		score.setScore(winKills);
		Score coins = objective.getScore(ChatColor.GOLD + "coins:");
		coins.setScore(0);
		Score pscore = objective.getScore(ChatColor.GOLD + "Your Score:");
		pscore.setScore(0);

		player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, PlayerXPHandler.getClass(player.getUniqueId()).getAttributes().getJumpBoost()));

		chatList.add(player);

		coinMap.put(player, 0);

		player.setScoreboard(playerBoard);
		objectiveMap.put(player, objective);
		player.setGameMode(GameMode.SURVIVAL);
		player.setHealth(20D);

		killCoinMap.put(player.getUniqueId(), coins);
		playerScoreMap.put(player.getUniqueId(), pscore);
		PerksHandler.newPlayer(player);
		player.getInventory().setContents(PlayerGearHandler.getClassInventory(PlayerXPHandler.getClass(player.getUniqueId()).getName()));
		setPlayerArmor(player);
		player.setFoodLevel(20);
		player.setSaturation(2F);

		return r;
	}

	public void noChat(Player player) {
		chatList.remove(player);
	}

	public boolean isRestarting() {
		return restarting;
	}

	public boolean isInvincible(UUID player) {
		if (invincible == null) {
			return false;
		}
		return invincible.contains(player);
	}

	public int getRespawnTime() {
		return respawnTime;
	}

	private Random rand = new Random();

	public boolean teleportPlayer(Player player) {
		return player.teleport(spawnLocations.get(rand.nextInt(spawnLocations.size())));
	}

	public void setPlayerArmor(Player player) {
		player.getInventory().setArmorContents(PlayerGearHandler.getClassArmor(PlayerXPHandler.getClass(player.getUniqueId()).getName()));
	}

	public void respawnPlayer(final Player player, int delay) {

		invincible.add(player.getUniqueId());
		player.setHealth(20D);
		player.setFireTicks(0);

		getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@Override
			public void run() {

				if (PerksHandler.hasPerk(player, Perk.LONGER_SPAWNPROTECT)) {
					PerksHandler.resetPerk(player, Perk.LONGER_SPAWNPROTECT);
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

						@Override
						public void run() {

							invincible.remove(player.getUniqueId());
							player.sendMessage(ChatColor.DARK_PURPLE + "Spawn protection wore off");
						}

					}, ticksInvincible + 100L);
				} else {
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

						@Override
						public void run() {

							invincible.remove(player.getUniqueId());
							player.sendMessage(ChatColor.DARK_PURPLE + "Spawn protection wore off");
						}

					}, ticksInvincible);
				}
				player.setFoodLevel(20);

				teleportPlayer(player);
				Utils.waitingToSpawn().remove(player.getUniqueId());
			}

		}, delay * 20));

		if (PerksHandler.hasPerk(player, Perk.KEEP_ITEMS)) {
			PerksHandler.resetPerk(player, Perk.KEEP_ITEMS);
		} else {
			player.getInventory().setContents(PlayerGearHandler.getClassInventory(PlayerXPHandler.getClass(player.getUniqueId()).getName()));
			setPlayerArmor(player);
		}

		if (delay > 0) {
			Utils.waitingToSpawn().add(player.getUniqueId());
			player.sendMessage(ChatColor.GOLD + "please wait " + delay + " seconds to respawn");
			player.teleport(Utils.waitArea());
		}

	}

	public void restart() {

		chatList.clear();
		restarting = true;
		kickedPlayers.clear();
		for (Player player : getPlayers()) {
			PerksHandler.removePlayer(player);
			player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		}
		for(Chest c: chests)
		{
		Utils.getChestList().remove(c);
		}
		chests.clear();
		players.clear();
		System.out.println("[UPVP] Unloading world: " + world.getName());

		MultiverseCore p = (MultiverseCore) plugin.getServer().getPluginManager().getPlugin("Multiverse-Core");
		
		//=====================================
		//UNLOAD WORLD IS HERE
		//=====================================
		p.getCore().getMVWorldManager().unloadWorld(world.getName());

		System.out.println("loading world:" + world.getName());
		plugin.getServer().createWorld(new WorldCreator(worldName).environment(environment));
		
		p.getCore().getMVWorldManager().loadWorld(world.getName());
		world = plugin.getServer().getWorld(world.getName());
		if (plugin.getServer().getWorld(worldName) == null) {
			Utils.logger.severe("SERVER WORLD WAS NULL!!!");
		}
		world.setAutoSave(false);

		// This sets the store chests back after
		// the world has been unloaded because they
		// would appear as empty chests and not
		// be counted as stores.
		checkChests(Utils.resetChests(name));

		restarting = false;
		plugin.getServer().broadcastMessage("Finished resetting PVP map");
		for (Location l : Utils.getSignsForGame(name)) {
			Sign sign = (Sign) plugin.getServer().getWorld(l.getWorld().getName()).getBlockAt(l).getState();
			sign.setLine(1, ChatColor.GREEN + "Join");
			sign.update();
		}
	}

	public void removeSpectator(Player player) {
		player.teleport(Utils.getSpawnLocation(player.getUniqueId()));
		player.removePotionEffect(PotionEffectType.INVISIBILITY);
		player.setCanPickupItems(true);
		player.setGameMode(GameMode.SURVIVAL);
		player.setWalkSpeed(1F);
	}

	public void calculateXP(UUID id) {
		PlayerXPHandler.addXP(id, (playerScoreMap.get(id).getScore() * 10));
	}

	// exciting!
	public void startGame() {

	}
}

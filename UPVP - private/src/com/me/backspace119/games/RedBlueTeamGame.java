package com.me.backspace119.games;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Score;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColls;
import com.massivecraft.factions.entity.UPlayer;
import com.me.backspace119.UltraPVP;
import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.games.objects.Team;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.factions.FactionXPHandler;
import com.me.backspace119.utils.handlers.player.PlayerGearHandler;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

/**
 * This class sets up a generic Red/Blue Team game that can be extended into a
 * more complex and specific game
 * 
 * @author backspace
 * 
 */
public class RedBlueTeamGame extends Game {
	// these need to be converted to protected...
	public Team blue;
	public Team red;
	protected Map<Player, String> teamMap = new HashMap<Player, String>();
	protected Location redLocation;
	protected Location blueLocation;
	protected ItemStack[] redArmor, blueArmor;
	protected Score blueKills;
	protected Score redKills;
	List<Team> teams = new ArrayList<Team>();

	public RedBlueTeamGame(String name, JavaPlugin plugin, Location redl, Location bluel, List<Location> spectatorLocations, int winKills, List<Block> chests, int ticksInvincible, ItemStack[] redArmor, ItemStack[] blueArmor, GameTypes type, World world, boolean hasWorld, String worldName) {
		super(name, plugin, spectatorLocations, winKills, chests, ticksInvincible, type, world, hasWorld, worldName, null);
		blue = new Team(bluel, winKills, ticksInvincible, this, ChatColor.BLUE, "BLUE");
		red = new Team(redl, winKills, ticksInvincible, this, ChatColor.RED, "RED");

		teams.add(blue);
		teams.add(red);
		redLocation = redl;
		blueLocation = bluel;
		this.redArmor = redArmor;
		this.blueArmor = blueArmor;

	}

	// add one to all team sizes to avoid divide by zero errors
	@Override
	public void calculateXP(UUID id) {
		
		if(getRedPlayers().size() != 0 && getBluePlayers().size() != 0)
		{
		if (PlayerXPHandler.getPerks(id).contains(Perk.DOUBLE_XP)) {
			if (red.getPlayers().contains(plugin.getServer().getPlayer(id))) {
				PlayerXPHandler.addXP(id, ((red.getTeamScore() / (red.getPlayers().size() + 1)) / 10) + getScore(id) * 5);
			} else {
				PlayerXPHandler.addXP(id, ((blue.getTeamScore() / (blue.getPlayers().size() + 1)) / 10) + getScore(id) * 5);

			}
		}
		if (red.getPlayers().contains(plugin.getServer().getPlayer(id))) {
			PlayerXPHandler.addXP(id, ((red.getTeamScore() / (red.getPlayers().size() + 1)) / 10) + getScore(id) * 5);
		} else {
			PlayerXPHandler.addXP(id, ((blue.getTeamScore() / (blue.getPlayers().size() + 1)) / 10) + getScore(id) * 5);

		}
		}
	}

	@Override
	public void endGame() {
		
		for (Player player : players) {
			calculateXP(player.getUniqueId());
		}
		for (Player player : getRedPlayers()) {

			Utils.getTagged().remove(player.getUniqueId());

			Utils.getInStoreList().remove(player.getUniqueId());
			player.setDisplayName(player.getName());

			player.getInventory().clear();
			player.getInventory().setArmorContents(new ItemStack[4]);
			for (PotionEffect e : player.getActivePotionEffects()) {
				player.removePotionEffect(e.getType());
			}
			if (PlayerXPHandler.getPerks(player.getUniqueId()).contains(Perk.DOUBLE_XP)) {
				if (UltraPVP.hasFactions) {
					UPlayer uplayer = UPlayer.get(player);
					Faction faction = uplayer.getFaction();
					if (!(faction == FactionColls.get().getForUniverse(uplayer.getUniverse()).getNone())) {
						FactionXPHandler.addXP(faction, (((red.getTeamScore() / red.getPlayers().size()) / 10) + getScore(player.getUniqueId())));
					}
				}
			}

			if (UltraPVP.hasFactions) {
				UPlayer uplayer = UPlayer.get(player);
				Faction faction = uplayer.getFaction();
				if (!(faction == FactionColls.get().getForUniverse(uplayer.getUniverse()).getNone())) {
					FactionXPHandler.addXP(faction, (((red.getTeamScore() / red.getPlayers().size()) / 10) + getScore(player.getUniqueId())));
				}
			}

			// the redundancy department called about the following 2 lines
			Utils.getGlobalCoins().put(player, Utils.getGlobalCoins().get(player) + red.getPlayerKills(player));
			PlayerXPHandler.setCoins(player.getUniqueId(), Utils.getGlobalCoins().get(player));
			player.setHealth(20D);
			player.setFoodLevel(20);
			player.setSaturation(2F);
			player.teleport(Utils.getSpawnLocation(player.getUniqueId()));

			Utils.removeFromGame(player);

		}
		for (Player player : getBluePlayers()) {
			Utils.getTagged().remove(player.getUniqueId());
			Utils.getInStoreList().remove(player.getUniqueId());
			player.setDisplayName(player.getName());

			player.getInventory().clear();
			player.getInventory().setArmorContents(new ItemStack[4]);
			for (PotionEffect e : player.getActivePotionEffects()) {
				player.removePotionEffect(e.getType());
			}
			if (PlayerXPHandler.getPerks(player.getUniqueId()).contains(Perk.DOUBLE_XP)) {

				if (UltraPVP.hasFactions) {
					UPlayer uplayer = UPlayer.get(player);
					Faction faction = uplayer.getFaction();
					if (!(faction == FactionColls.get().getForUniverse(uplayer.getUniverse()).getNone())) {
						FactionXPHandler.addXP(faction, (((blue.getTeamScore() / blue.getPlayers().size()) / 10) + getScore(player.getUniqueId())));
					}
				}
			}
			if (UltraPVP.hasFactions) {
				UPlayer uplayer = UPlayer.get(player);
				Faction faction = uplayer.getFaction();
				if (!(faction == FactionColls.get().getForUniverse(uplayer.getUniverse()).getNone())) {
					FactionXPHandler.addXP(faction, (((blue.getTeamScore() / blue.getPlayers().size()) / 10)));
				}
			}
			Utils.getGlobalCoins().put(player, Utils.getGlobalCoins().get(player) + blue.getPlayerKills(player));
			PlayerXPHandler.setCoins(player.getUniqueId(), Utils.getGlobalCoins().get(player));
			player.setHealth(20D);
			player.setFoodLevel(20);
			player.setSaturation(2F);
			player.teleport(Utils.getSpawnLocation(player.getUniqueId()));

			Utils.removeFromGame(player);
		}
		PlayerXPHandler.save();
		super.endGame();
		blue.reset();
		red.reset();
		Utils.updateSignsForTeamGame(name);

	}

	private Random rand = new Random();

	@Override
	public boolean teleportPlayer(Player player) {
		if (getBluePlayers().contains(player)) {

			return player.teleport(new Location(blueLocation.getWorld(), blueLocation.getX() + rand.nextInt(3), blueLocation.getY(), blueLocation.getZ() + rand.nextInt(3)));

		} else {
			return player.teleport(new Location(redLocation.getWorld(), redLocation.getX() + rand.nextInt(3), redLocation.getY(), redLocation.getZ() + rand.nextInt(3)));
		}
	}

	@Override
	public void setPlayerArmor(Player player) {
		ItemStack[] armor;
		armor = PlayerGearHandler.getClassArmor(PlayerXPHandler.getClass(player.getUniqueId()).getName());
		if (getBluePlayers().contains(player)) {
			armor[3] = getBlueArmor()[3];
		} else {
			armor[3] = getRedArmor()[3];
		}

		player.getInventory().setArmorContents(armor);
	}

	@Override
	public void respawnPlayer(Player player, int delay) {
		super.respawnPlayer(player, delay);

	}

	public boolean isOnSameTeam(Player player1, Player player2) {
		return getRedPlayers().contains(player1) && getRedPlayers().contains(player2) || getBluePlayers().contains(player1) && getBluePlayers().contains(player2);
	}

	public void teamWonWithPlayer(Player winner) {
		System.out.println("[DEBUG] teamWonWithPlayer called");
		for (Player player : getBluePlayers()) {
			player.teleport(blueLocation);

		}
		for (Player player : getRedPlayers()) {
			player.teleport(redLocation);
		}
		// endGame();

		getTeam(winner.getUniqueId()).setTeamScore(blue.getTeamScore() + 1000);
		for (Player player : getTeam(winner.getUniqueId()).getPlayers()) {
			PlayerXPHandler.addWin(player.getUniqueId());
		}
		for (Player player : getOpposingTeam(winner.getUniqueId()).getPlayers()) {
			PlayerXPHandler.addLoss(player.getUniqueId());
		}
		broadcastAll(getTeam(winner.getUniqueId()).teamColor + getTeam(winner.getUniqueId()).getName() + " HAS WON!");

		endGame();

	}

	public void teamWonWithoutPlayer() {

		for (Player player : getBluePlayers()) {
			player.teleport(blueLocation);

		}
		for (Player player : getRedPlayers()) {
			player.teleport(redLocation);
		}
		// endGame();
		if (blue.getTeamKills() > red.getTeamKills()) {
			for (Player player : getBluePlayers()) {
				PlayerXPHandler.addWin(player.getUniqueId());
			}
			for (Player player : getRedPlayers()) {
				PlayerXPHandler.addLoss(player.getUniqueId());
			}
			blue.setTeamScore(blue.getTeamScore() + 1000);
			broadcastAll(ChatColor.BLUE + "BLUE HAS WON!");

		} else if (blue.getTeamKills() < red.getTeamKills()) {
			for (Player player : getRedPlayers()) {
				PlayerXPHandler.addWin(player.getUniqueId());
			}
			for (Player player : getBluePlayers()) {
				PlayerXPHandler.addLoss(player.getUniqueId());
			}
			red.setTeamScore(red.getTeamScore() + 1000);

			broadcastAll(ChatColor.RED + "RED HAS WON!");

		} else {
			for (Player player : getRedPlayers()) {
				PlayerXPHandler.addTie(player.getUniqueId());
			}
			for (Player player : getBluePlayers()) {
				PlayerXPHandler.addTie(player.getUniqueId());
			}
			broadcastAll(ChatColor.GOLD + "Its a tie!");
			red.setTeamScore(red.getTeamScore() + 500);
			blue.setTeamScore(blue.getTeamScore() + 500);

		}

		endGame();
	}

	public List<Player> getRedPlayers() {
		return red.getPlayers();
	}

	public List<Player> getBluePlayers() {
		return blue.getPlayers();
	}

	@Override
	@SuppressWarnings("deprecation")
	public boolean joinGame(Player player, String prefrence) {
		boolean r;
		super.joinGame(player, prefrence);
		blueKills = addScoreboardInfo(player, (ChatColor.BLUE + "BlueKills:"), 0);
		redKills = addScoreboardInfo(player, (ChatColor.RED + "RedKills:"), 0);

		if (prefrence.equalsIgnoreCase("RED")) {
			r = player.teleport(redLocation);
			broadcast(ChatColor.RED + player.getName() + " Has joined Red!");
			player.sendMessage(ChatColor.RED + "You are on Red!");

			player.setDisplayName(ChatColor.RED + player.getName() + " " + PlayerXPHandler.getClass(player.getUniqueId()).getName());
			plugin.getServer().getPlayer(player.getUniqueId()).updateInventory();
			red.addPlayer(player, redKills);
			blue.addScore(blueKills);

			if (getRedPlayers().size() < 1) {

				getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

					@Override
					public void run() {

						teamWonWithoutPlayer();

					}

				}, 36000L));
				getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

					@Override
					public void run() {

						broadcastAll(ChatColor.GOLD + "==================================");
						broadcastAll(ChatColor.GOLD + "GAME ENDS IN 1 MINUTE!");
						broadcastAll(ChatColor.GOLD + "==================================");

					}

				}, 34800L));
			}
			Utils.updateSignsForTeamGame(name);

		} else if (prefrence.equalsIgnoreCase("BLUE")) {
			r = player.teleport(blueLocation);
			broadcast(ChatColor.BLUE + player.getName() + " Has joined Blue!");
			player.sendMessage(ChatColor.BLUE + "You are on Blue!");

			player.setDisplayName(ChatColor.BLUE + player.getName() + " " + PlayerXPHandler.getClass(player.getUniqueId()).getName());
			plugin.getServer().getPlayer(player.getUniqueId()).updateInventory();
			blue.addPlayer(player, blueKills);
			red.addScore(redKills);
			for (Location l : Utils.getSignsForGame(name)) {
				Sign sign = (Sign) plugin.getServer().getWorld(l.getWorld().getName()).getBlockAt(l).getState();
				sign.setLine(3, ChatColor.RED + "" + getRedPlayers().size() + ChatColor.BLACK + "/" + ChatColor.BLUE + "" + getBluePlayers().size());
				sign.update();
			}

		} else if (getBluePlayers().size() > getRedPlayers().size() || red.getTeamScore() < blue.getTeamScore()) {
			r = player.teleport(redLocation);
			broadcast(ChatColor.RED + player.getName() + " Has joined Red!");
			player.sendMessage(ChatColor.RED + "You are on Red!");

			player.setDisplayName(ChatColor.RED + player.getName() + " " + PlayerXPHandler.getClass(player.getUniqueId()).getName());
			red.addPlayer(player, redKills);
			blue.addScore(blueKills);
			plugin.getServer().getPlayer(player.getUniqueId()).updateInventory();
			if (getRedPlayers().size() < 1) {

				getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

					@Override
					public void run() {

						teamWonWithoutPlayer();

					}

				}, 36000L));
				getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

					@Override
					public void run() {

						broadcastAll(ChatColor.GOLD + "==================================");
						broadcastAll(ChatColor.GOLD + "GAME ENDS IN 1 MINUTE!");
						broadcastAll(ChatColor.GOLD + "==================================");

					}

				}, 34800L));
			}
			Utils.updateSignsForTeamGame(name);

		} else {

			r = player.teleport(blueLocation);
			broadcast(ChatColor.BLUE + player.getName() + " Has joined Blue!");
			player.sendMessage(ChatColor.BLUE + "You are on Blue!");

			player.setDisplayName(ChatColor.BLUE + player.getName() + " " + PlayerXPHandler.getClass(player.getUniqueId()).getName());
			blue.addPlayer(player, blueKills);
			red.addScore(redKills);
			Utils.updateSignsForTeamGame(name);
			plugin.getServer().getPlayer(player.getUniqueId()).updateInventory();

		}

		player.sendMessage(ChatColor.GOLD + "Welcome to the game and good luck. Type /upvp nochat to stop the chat if its too spammy for you.");

		if (!PlayerXPHandler.config.getConfig().contains(player.getUniqueId() + ".class")) {
			PlayerXPHandler.setClass(player.getUniqueId(), PlayerXPHandler.getClasses().get("Soldier"));
		}

		if (PlayerXPHandler.getClass(player.getUniqueId()).getName().equalsIgnoreCase("Tank")) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1));

		} else if (PlayerXPHandler.getClass(player.getUniqueId()).getName().equalsIgnoreCase("Spy")) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 1));
		}
		player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, PlayerXPHandler.getClass(player.getUniqueId()).getAttributes().getJumpBoost()));

		setPlayerArmor(player);
		plugin.getServer().getPlayer(player.getUniqueId()).updateInventory();

		return r;
	}

	public ItemStack[] getRedArmor() {
		return redArmor;
	}

	public ItemStack[] getBlueArmor() {
		return blueArmor;
	}

	@Override
	public void restart() {
		super.restart();
		blueLocation.setWorld(plugin.getServer().getWorld(worldName));
		redLocation.setWorld(plugin.getServer().getWorld(worldName));
	}

	/**
	 * i.e. they drowned, were esplodded, fell from orbit, burned to death, etc.
	 * (all of these cannot return who it was that killed them, or at least they
	 * don't right now) removes a kill from that team. This should possibly be
	 * more specific for this type of two team game as it is set up to be used
	 * in even multi team games
	 * 
	 * @param id
	 *            player's UUID
	 */
	public void playerDiedNoKiller(UUID id) {
		getTeam(id).removeKill();
		broadcast(getTeam(id).teamColor + plugin.getServer().getPlayer(id).getName() + " has been killed!");
	}

	/**
	 * this is fitting of a multi team (more than two) game. This should
	 * possibly be more specific for this game
	 * 
	 * @param id
	 *            player's UUID
	 * @return team they are on, Red or Blue
	 */
	public Team getTeam(UUID id) {
		for (Team t : teams) {
			if (t.getPlayers().contains(plugin.getServer().getPlayer(id))) { return t; }
		}

		return null;
	}
	
	public Team getOpposingTeam(UUID id)
	{
		if(!red.getPlayers().contains(plugin.getServer().getPlayer(id)))
		{
			return red;
		}else{
			return blue;
		}
	}

}

package com.me.backspace119.games.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Score;

import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.games.RedBlueTeamGame;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.player.PerksHandler;

public class Team {
	// CODE HAS BEEN CLEANED! NEW CODE SHOULD STAY CLEAN!

	public final ChatColor teamColor;
	List<Player> players = new ArrayList<Player>();
	Map<UUID, Long> lastDeathMap = new HashMap<UUID, Long>();
	int teamScore;
	Map<Player, Integer> scoreMap = new HashMap<Player, Integer>();
	int teamKills;
	Map<Player, Integer> killMap = new HashMap<Player, Integer>();
	Location respawn;
	int killLimit;
	List<Score> teamScoreboards = new ArrayList<Score>();
	int streakTime;
	RedBlueTeamGame game;
	private final String name;

	public Team(Location location, int killLimit, int streakTime, RedBlueTeamGame game, ChatColor teamColor, String name) {
		respawn = location;
		this.killLimit = killLimit;
		this.game = game;
		this.streakTime = streakTime;
		this.teamColor = teamColor;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void addPlayer(Player player, Score score) {
		players.add(player);
		killMap.put(player, 0);
		scoreMap.put(player, 0);
		teamScoreboards.add(score);

	}

	public void removePlayer(Player player) {
		players.remove(player);
	}

	public int getTeamScore() {
		return teamScore;
	}

	public void setTeamScore(int score) {
		teamScore = score;
	}

	public int getTeamKills() {
		return teamKills;
	}

	public int getPlayerKills(Player player) {
		return killMap.get(player);
	}

	public void addSuicide(Player suicider) {
		if (teamKills > 0) {
			teamKills--;
		}
		for (Score score : teamScoreboards) {
			score.setScore(teamKills);
		}
		killMap.put(suicider, killMap.get(suicider) - 1);

		if (killMap.get(suicider) < -3) {
			Utils.kickPlayer(suicider.getUniqueId());
			return;
		}
		suicider.sendMessage("continued suiciding will kick you from the game. " + (killMap.get(suicider) + 3) + " chance(s) left");
		suicider.setHealth(20D);
	}

	public void addScore(Score score) {
		teamScoreboards.add(score);
	}

	public void addKill(Player killer, Player killed) {
		System.out.println("[DEBUG] Adding a kill!");
		if (!lastDeathMap.containsKey(killed.getUniqueId())) {
			lastDeathMap.put(killed.getUniqueId(), 0L);
		}
		if (lastDeathMap.get(killed.getUniqueId()) < (System.currentTimeMillis() - 1000)) {

			lastDeathMap.put(killed.getUniqueId(), System.currentTimeMillis());
			if (PerksHandler.hasPerk(killer, Perk.DOUBLE_COINS)) {
				Utils.getGlobalCoins().put(killer, Utils.getGlobalCoins().get(killer) + 2);
				game.getCoins().put(killer, game.getCoins().get(killer) + 2);
				game.updateCoinBoard(killer.getUniqueId());
				game.addPoint(killer.getUniqueId());
				PerksHandler.resetPerk(killer, Perk.DOUBLE_COINS);
			} else {
				Utils.getGlobalCoins().put(killer, Utils.getGlobalCoins().get(killer) + 1);
				game.getCoins().put(killer, game.getCoins().get(killer) + 1);
				game.updateCoinBoard(killer.getUniqueId());
				game.addPoint(killer.getUniqueId());
			}
			teamScore += 5;
			scoreMap.put(killer, scoreMap.get(killer) + 5);
			teamKills++;
			killMap.put(killer, killMap.get(killer) + 1);
			if ((killLimit < teamKills || killLimit == teamKills) && game.getType() != GameTypes.CONTROLPOINT) {
				game.teamWonWithPlayer(killer);
				return;
			}

			game.broadcast(teamColor + killer.getName() + " has killed " + game.getTeam(killed.getUniqueId()).teamColor + killed.getName());

			for (Score score : teamScoreboards) {
				score.setScore(teamKills);
			}
		} else {
			System.out.println("Prevented a lag double kill on: " + killed.getName());
		}

	}

	public void reset() {
		teamKills = 0;
		killMap.clear();
		scoreMap.clear();
		teamScore = 0;
		players.clear();
	}

	/**
	 * usually called when a player is killed and the identity of the killer
	 * cannot be found this keeps everything fair in games with more than two
	 * teams
	 */
	public void removeKill() {
		teamKills--;
		for (Score score : teamScoreboards) {
			score.setScore(teamKills);
		}

	}

	// this should be part of the game object not the team object
	public void setKillLimit(int killLimit) {
		this.killLimit = killLimit;
	}

}

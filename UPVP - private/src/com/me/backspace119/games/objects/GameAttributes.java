package com.me.backspace119.games.objects;

import java.util.UUID;

import org.bukkit.entity.Entity;

/**
 * This will house all the methods for differentiating one game type from
 * another and the GameTypes enum will die. This makes games dynamic and anyone
 * can make their own gametypes by using this plugin as a library in their own
 * 
 * NOT IMPLEMENTED AS OF VERSION 1.5.X planned for 1.6
 * 
 * @author backspace
 * 
 */
public interface GameAttributes {

	public void onWin();

	public void checkWin();

	public void onKilledByPlayer(UUID killer, UUID killed);

	public void onKilledByEntity(UUID killed, Entity entity);

	public void onKilledByOther(UUID killed);

}

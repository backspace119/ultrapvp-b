package com.me.backspace119.games;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.UPlayer;
import com.me.backspace119.enums.FactionPerk;
import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.utils.handlers.factions.FactionXPHandler;

public class FactionGame extends RedBlueTeamGame {

	Faction f1;
	Faction f2;
	String factionName1 = "";
	String factionName2 = "";
	GameTypes type = GameTypes.FACTIONMATCH;

	public FactionGame(String name, JavaPlugin plugin, Location redl, Location bluel, List<Location> spectatorLocations, int winKills, List<Block> chests, int ticksInvincible, ItemStack[] redArmor, ItemStack[] blueArmor, GameTypes type, World world, boolean hasWorld) {

		super(name, plugin, redl, bluel, spectatorLocations, winKills, chests, ticksInvincible, redArmor, blueArmor, type, world, hasWorld, world.getName());

	}

	/**
	 * handles faction perks here to make it more centralized OBVIOUSLY DOESN'T
	 * WORK YET
	 * 
	 * @param player
	 *            the player who issued the perk
	 * @param perk
	 *            the perk that dun got used
	 */
	public void useFactionPerk(Player player, FactionPerk perk) {
		UPlayer uplayer = UPlayer.get(player);
		Faction faction = uplayer.getFaction();

	}

	public String getFactionNameForPlayer(Player player) {
		if (getRedPlayers().contains(player)) {
			return factionName1;
		} else {
			return factionName2;
		}
	}

	public List<Player> getPlayersByFaction(String name) {
		if (name.equalsIgnoreCase(factionName1)) {
			return getRedPlayers();
		} else {
			return getBluePlayers();
		}
	}

	@Override
	public boolean joinGame(Player player, String prefrence) {

		UPlayer uplayer = UPlayer.get(player);
		Faction faction = uplayer.getFaction();

		if (factionName1.equalsIgnoreCase("") && factionName2.equalsIgnoreCase("")) {
			factionName1 = faction.getName();
			f1 = faction;
			return super.joinGame(player, "RED");
		} else if (factionName2.equalsIgnoreCase("")) {
			if (!faction.getName().equals(factionName1)) {
				factionName2 = faction.getName();
				f2 = faction;
				return super.joinGame(player, "BLUE");
			}
		} else {
			if (faction.getName().equalsIgnoreCase(factionName1)) {
				return super.joinGame(player, "RED");
			} else if (faction.getName().equalsIgnoreCase(factionName2)) { return super.joinGame(player, "BLUE"); }
		}

		return false;
	}

	@Override
	public void teamWonWithPlayer(Player player) {
		if (getRedPlayers().contains(player)) {
			FactionXPHandler.addXP(f1, 1000);
		} else {
			FactionXPHandler.addXP(f2, 1000);
		}
		super.teamWonWithPlayer(player);

	}

}

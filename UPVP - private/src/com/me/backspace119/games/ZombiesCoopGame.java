package com.me.backspace119.games;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

public class ZombiesCoopGame extends Game {

	protected List<Location> zombieSpawnLocations;
	protected int difficulty;
	protected List<Entity> zombies;
	protected int wave;
	protected List<UUID> injured = new ArrayList<UUID>();
	protected Chest medChest;

	public ZombiesCoopGame(String name, JavaPlugin plugin, List<Location> spectatorLocations, int winKills, List<Block> chests, int ticksInvincible, GameTypes type, World world, boolean hasWorld, String worldName, List<Location> spawnLocations, List<Location> zombieSpawnLocations, int difficulty, Chest medChest) {
		super(name, plugin, spectatorLocations, winKills, chests, ticksInvincible, type, world, hasWorld, worldName, spawnLocations);
		this.zombieSpawnLocations = zombieSpawnLocations;
		this.difficulty = difficulty;
		zombies = new ArrayList<Entity>();
		world.setDifficulty(Difficulty.HARD);
		wave = 0;
		this.medChest = medChest;
		world.setMonsterSpawnLimit(0);
	}

	@Override
	public boolean joinGame(Player player, String prefrence) {
		if (players.size() == 0) {
			startGame();
		}
		super.joinGame(player, prefrence);

		return true;
	}

	@Override
	public void startGame() {

		ItemStack medStick = new ItemStack(Material.STICK);
		medStick.getItemMeta().setDisplayName("Healing Rod");
		medStick.setAmount(15);
		medChest.getInventory().clear();
		medChest.getInventory().setItem(0, medStick);
		nextWave();

	}

	public void addZombie() {
		Entity zombie = world.spawnEntity(zombieSpawnLocations.get(rand.nextInt(zombieSpawnLocations.size())), EntityType.ZOMBIE);
		zombies.add(zombie);
	}

	@Override
	public void calculateXP(UUID name) {
		super.calculateXP(name);
		PlayerXPHandler.addXP(name, wave * 5);
	}

	@Override
	public void respawnPlayer(final Player player, int delay) {
		broadcast(player.getName() + " - " + ChatColor.YELLOW + "I'm down mates! I need medical assitance!" + ChatColor.RED + " You have 15 seconds to save them!");
		getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			@Override
			public void run() {
				broadcast(ChatColor.DARK_BLUE + player.getName() + ChatColor.DARK_RED + " - I wish you the best comrades, but i believe this has been my last fight!");
				broadcast(ChatColor.RED + player.getName() + " has died! Continue the fight and try not to lose anymore players!");

				killPlayer(player);
			}
		}, (15 * 20)));
		player.setGameMode(GameMode.CREATIVE);
		player.setCanPickupItems(false);
		player.setWalkSpeed(0F);
		player.setAllowFlight(false);
		injured.add(player.getUniqueId());

	}

	@Override
	public void endGame() {
		super.endGame();
		world.setMonsterSpawnLimit(0);
	}

	public void revivePlayer(Player player, Player revivor) {
		player.setGameMode(GameMode.SURVIVAL);
		player.setWalkSpeed(1F);
		player.setCanPickupItems(true);

		addPoint(revivor.getUniqueId());
		addPoint(revivor.getUniqueId());
		injured.remove(player.getUniqueId());
		revivor.sendMessage(ChatColor.GREEN + "You receive 2 points for reviving a comrade");
		broadcast(ChatColor.WHITE + revivor.getName() + ChatColor.GREEN + " has revived " + ChatColor.WHITE + player.getName());

	}

	public List<UUID> getInjured() {
		return injured;
	}

	public void killPlayer(Player player) {
		player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, Integer.MAX_VALUE));
		player.setWalkSpeed(0F);
		player.setCanPickupItems(false);
		spectators.add(player);
		players.remove(player);
		calculateXP(player.getUniqueId());

	}

	Random rand = new Random();

	public void nextWave() {
		world.setTime((10 * 60) * 22);
		world.setMonsterSpawnLimit(1);
		broadcast(ChatColor.RED + "HERE COMES THE NEXT WAVE! Next wave in " + (25 + wave + 1) + " seconds!");
		for (int i = 0; i < (players.size() * (difficulty + wave)); i++) {
			addZombie();
			world.setMonsterSpawnLimit(zombies.size() + 1);
		}
		wave++;

		getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			@Override
			public void run() {
				nextWave();
			}
		}, ((25 + wave) * 20)));
		world.setMonsterSpawnLimit(zombies.size());

	}

	public void zombieKilled(Entity zombie, UUID killer) {
		addPoint(killer);
		zombies.remove(zombie);
		world.setMonsterSpawnLimit(zombies.size());
		if (zombies.isEmpty()) {
			broadcast(ChatColor.GREEN + "The wave has been wiped out! Prepare for the next wave!");
		}

	}

}

package com.me.backspace119.events;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.games.ControlPointGame;
import com.me.backspace119.utils.Utils;

public class BlockEventHandler implements Listener {

	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		if (event.getLine(0).equals("[UPVP]")) {
			if (event.getPlayer().hasPermission("upvp.createSign")) {
				if (event.getLine(1).equals("") || event.getLine(1) == null) {
					event.getBlock().breakNaturally();
					event.getPlayer().sendMessage(ChatColor.RED + "Please specify game arena name on line 2 of the sign");
				} else if (Utils.getGameMap().get(event.getLine(1)) != null) {

					if (event.getLine(2).equalsIgnoreCase("spectate")) {
						event.setLine(2, event.getLine(1));
						event.setLine(1, ChatColor.BLUE + "Spectate");

					} else {
						List<Location> signs;
						if (Utils.getSignsForGame(event.getLine(1)) != null) {
							signs = Utils.getSignsForGame(event.getLine(1));
						} else {

							signs = new ArrayList<Location>();
						}

						signs.add(event.getBlock().getLocation());
						Utils.setSignsForGame(event.getLine(1), signs);

						event.setLine(0, ChatColor.GOLD + "[UPVP]");
						event.setLine(2, event.getLine(1));
						event.setLine(1, ChatColor.GREEN + "Join");
						// Utils.getSignMap().put(event.getBlock(),
						// Utils.getGameMap().get(event.getLine(2)));
					}
				} else {
					event.getBlock().breakNaturally();
					event.getPlayer().sendMessage(ChatColor.RED + "No such game exists");
				}

			} else {
				event.getPlayer().sendMessage(ChatColor.RED + "You do not have permission to make a sign");
			}
		}
	}

	@EventHandler
	public void onBlockInteract(PlayerInteractEvent event) {

		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = event.getClickedBlock();
			if (b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST) {
				Sign sign = (Sign) b.getState();
				String[] lines = sign.getLines();
				if (lines[0].equals(ChatColor.GOLD + "[UPVP]")) {
					if (lines[1].equalsIgnoreCase("spectate")) {
						Utils.getGameMap().get(lines[2]).joinAsSpectator(event.getPlayer());
					} else if (Utils.getSignsForGame(lines[2]) != null) {
						if (!Utils.getSignsForGame(lines[2]).contains(sign.getLocation())) {
							List<Location> signs = Utils.getSignsForGame(lines[2]);
							signs.add(sign.getLocation());
							Utils.setSignsForGame(lines[2], signs);
						}
					} else {
						List<Location> signs = new ArrayList<Location>();
						signs.add(sign.getLocation());
						Utils.setSignsForGame(lines[2], signs);
					}
					/*for(Location l: Utils.getSignsForGame(lines[2]))
					{
						System.out.println("location shit: " + l.getX() + " " + l.getY() + " " + l.getZ() + " ");
					}*/

					Utils.joinGame(event.getPlayer(), lines[2]);
				}
			} else if (b.getType() == Material.CHEST) {

				Chest chest = (Chest) b.getState();
				if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
					if (Utils.getGameMap().get(Utils.getPlayerMap().get(event.getPlayer())).getChests().contains(chest)) {

						if (!Utils.getInStoreList().contains(event.getPlayer().getUniqueId())) {
							chest.update();
							Utils.getInStoreList().add(event.getPlayer().getUniqueId());

						}
					}
				}
			}
		}

	}

	static Block b;

	@EventHandler
	public void explodeEvent(EntityExplodeEvent event) {

		Iterator<Block> iter = event.blockList().iterator();
		while (iter.hasNext()) {

			b = iter.next();

			if (Utils.getChestList().contains(b.getLocation())) {

				iter.remove();
			}

		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void blockBreakEvent(BlockBreakEvent event) {

		if (Utils.getSignMap().containsKey(event.getBlock())) {
			Utils.getSignMap().remove(event.getBlock());
		}
		if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
			if (Utils.getGameByPlayer(event.getPlayer()).getSpectators().contains(event.getPlayer())) {
				event.setCancelled(true);
				return;
			}
			if (event.getBlock().getType().equals(Material.CHEST)) {
				if (Utils.getGameMap().get(Utils.getPlayerMap().get(event.getPlayer())).getChests().contains(event.getBlock().getState())) {
					event.setCancelled(true);
					return;
				}
			} else if (Utils.getGameMap().get(Utils.getPlayerMap().get(event.getPlayer())).getType().equals(GameTypes.CONTROLPOINT)) {
				ControlPointGame game = (ControlPointGame) Utils.getGameByPlayer(event.getPlayer());
				if (game.getRedPlayers().contains(event.getPlayer())) {
					if (game.getRedPoints().contains(event.getBlock())) {
						if (game.getBlueControls().contains(event.getBlock())) {
							Utils.getGameByPlayer(event.getPlayer()).broadcastAll(ChatColor.RED + event.getPlayer().getName() + ChatColor.GOLD + " has recaptured a point!");
							game.getRedControls().add(event.getBlock());
							game.getBlueControls().remove(event.getBlock());
							event.getBlock().setType(Material.WOOL);
							event.getBlock().setData((byte) 14);
							// Wool wool = (Wool) event.getBlock();
							// wool.setColor(DyeColor.RED);
							game.addCapture(event.getPlayer());
							if (game.checkWin()) {
								game.teamWonWithPlayer(event.getPlayer());
							}
						}
						event.setCancelled(true);
						return;
					} else if (game.getBluePoints().contains(event.getBlock())) {
						if (game.getBlueControls().contains(event.getBlock())) {
							game.broadcastAll(ChatColor.RED + event.getPlayer().getName() + " has captured a point! " + (game.getBlueControls().size() - 1) + " to go");
							game.getRedControls().add(event.getBlock());
							game.getBlueControls().remove(event.getBlock());
							event.getBlock().setType(Material.WOOL);
							event.getBlock().setData((byte) 14);
							// Wool wool = (Wool) event.getBlock();
							// wool.setColor(DyeColor.RED);
							game.addCapture(event.getPlayer());
							if (game.checkWin()) {
								game.teamWonWithPlayer(event.getPlayer());
							}
							event.setCancelled(true);
							return;
						} else {
							event.setCancelled(true);
							return;
						}

					}
				} else {
					if (game.getBluePoints().contains(event.getBlock())) {
						if (game.getRedControls().contains(event.getBlock())) {
							Utils.getGameByPlayer(event.getPlayer()).broadcastAll(ChatColor.BLUE + event.getPlayer().getName() + ChatColor.GOLD + " has recaptured a point!");
							game.getBlueControls().add(event.getBlock());
							game.getRedControls().remove(event.getBlock());
							event.getBlock().setType(Material.WOOL);
							event.getBlock().setData((byte) 11);
							// Wool wool = (Wool) event.getBlock();
							// wool.setColor(DyeColor.BLUE);

							game.addCapture(event.getPlayer());
							if (game.checkWin()) {
								game.teamWonWithPlayer(event.getPlayer());
							}
							event.setCancelled(true);
							return;
						} else {
							event.setCancelled(true);
							return;
						}
					} else if (game.getRedPoints().contains(event.getBlock())) {
						if (game.getRedControls().contains(event.getBlock())) {
							game.broadcastAll(ChatColor.BLUE + event.getPlayer().getName() + " has captured a point! " + (game.getRedControls().size() - 1) + " to go");
							game.getBlueControls().add(event.getBlock());
							game.getRedControls().remove(event.getBlock());
							event.getBlock().setType(Material.WOOL);
							event.getBlock().setData((byte) 11);
							// Wool wool = (Wool) event.getBlock();
							// wool.setColor(DyeColor.BLUE);

							game.addCapture(event.getPlayer());
							if (game.checkWin()) {
								game.teamWonWithPlayer(event.getPlayer());
							}
							event.setCancelled(true);
							return;
						} else {
							event.setCancelled(true);
							return;
						}
					}
				}
			}
			event.setCancelled(false);
		}
	}
}

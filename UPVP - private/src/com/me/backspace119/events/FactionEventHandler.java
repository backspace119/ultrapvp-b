package com.me.backspace119.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.massivecraft.factions.event.FactionsEventCreate;
import com.massivecraft.factions.event.FactionsEventNameChange;
import com.me.backspace119.utils.handlers.factions.FactionXPHandler;

public class FactionEventHandler implements Listener {

	@EventHandler
	public void onFactionCreate(FactionsEventCreate event) {
		FactionXPHandler.newFaction(event.getFactionName());
	}

	@EventHandler
	public void onFactionNameChange(FactionsEventNameChange event) {
		FactionXPHandler.changeName(event.getFaction().getName(), event.getNewName());
	}
}

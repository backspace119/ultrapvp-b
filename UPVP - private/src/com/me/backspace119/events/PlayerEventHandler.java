package com.me.backspace119.events;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.EntityEffect;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import com.me.backspace119.api.PVPPlayerKilledEvent;
import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.games.RedBlueTeamGame;
import com.me.backspace119.games.ZombiesCoopGame;
import com.me.backspace119.utils.Messaging;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.ConfigHandler;
import com.me.backspace119.utils.handlers.ShopInventoryHandler;
import com.me.backspace119.utils.handlers.player.PerksHandler;
import com.me.backspace119.utils.handlers.player.PlayerGearHandler;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

public class PlayerEventHandler implements Listener {

	ConfigHandler messagingConfig;
	JavaPlugin plugin;

	public PlayerEventHandler(ConfigHandler messagingConfig, JavaPlugin plugin) {
		this.messagingConfig = messagingConfig;
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
			event.setCancelled(false);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
			if (!event.getMessage().contains("/upvp") && !event.getMessage().contains("/v")) {
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.GOLD + "You may not use non-upvp commands while in game. To use commands, wait for the game to finish or leave the game with /upvp leave");
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
			event.setCancelled(true);
			Utils.getGameByPlayer(event.getPlayer()).broadcast(event.getPlayer().getDisplayName() + " - " + ChatColor.WHITE + event.getMessage());
		} else if (Utils.isMakingClass(event.getPlayer().getUniqueId())) {
			PlayerXPHandler.getClasses().get(Utils.getMakingClass(event.getPlayer().getUniqueId())).setDescription(event.getMessage());
			Utils.setMakingClass(event.getPlayer().getUniqueId(), false, null);
			PlayerXPHandler.getClasses().get(Utils.getMakingClass(event.getPlayer().getUniqueId())).save();
			PlayerGearHandler.setClassGear(Utils.getMakingClass(event.getPlayer().getUniqueId()), event.getPlayer().getUniqueId());
		}
	}

	@EventHandler
	public void onPlayerKilled(PVPPlayerKilledEvent event) {

		// event thrown even if player was killed and no killer could be found
		// this means that perks can be null in this case
		if (event.getPerk() != null) {
			if (PerksHandler.hasPerk(event.getKiller(), Perk.HEADS_UP)) {
				PlayerXPHandler.addHead(event.getKiller().getUniqueId(), event.getKilled().getName());
				event.getKiller().sendMessage(ChatColor.AQUA + "You have cut off your opponent's head and stored it for safe keeping! You may retrieve it later with /upvp getHeads");
				PerksHandler.resetPerk(event.getKiller(), Perk.HEADS_UP);
			}
		}
	}

	PVPPlayerKilledEvent pke;

	/**
	 * This handles when a player is damaged by another player in game
	 * 
	 * @param event
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerHit(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			Player damager = (Player) event.getDamager();

			if (Utils.getPlayerMap().containsKey(damager)) {
				if (Utils.getGameByPlayer(damager).getType().equals(GameTypes.COOP_ZOMBIES)) {
					onHitZombieCoop(event, damager);
					return;
				}
			}
			if (event.getEntity() instanceof Player) {
				Player damaged = (Player) event.getEntity();

				if (Utils.getPlayerMap().containsKey(damager) && Utils.getPlayerMap().containsKey(damaged)) {

					if (Utils.getGameByPlayer(damaged).getType().isTeam()) {
						onHitTeamGame(event, (RedBlueTeamGame) Utils.getGameByPlayer(damaged));
					}
				}
			} else {

			}
		} else if (event.getDamager() instanceof Zombie) {
			Zombie zombie = (Zombie) event.getDamager();
			if (event.getEntity() instanceof Player) {
				Player damaged = (Player) event.getEntity();
				if (Utils.getPlayerMap().containsKey(damaged)) {

					if (Utils.getGameByPlayer(damaged).getType().equals(GameTypes.COOP_ZOMBIES)) {
						onZombieHitCoop(event, zombie, damaged);
					}
				}
			}
		} else if (event.getDamager() instanceof Projectile) {
			if (event.getEntity() instanceof Player) {
				Player damaged = (Player) event.getEntity();

				if (Utils.getPlayerMap().containsKey(damaged)) {
					if (Utils.getGameByPlayer(damaged).getType().isTeam()) {
						RedBlueTeamGame game = (RedBlueTeamGame) Utils.getGameByPlayer(damaged);
						onHitTeamGame(event, game);
					}
				}
			}
		}

	}

	public void onZombieHitCoop(EntityDamageByEntityEvent event, Zombie damager, Player damaged) {
		ZombiesCoopGame game = (ZombiesCoopGame) Utils.getGameByPlayer(damaged);
		if (damaged.getHealth() - event.getDamage() < 1) {
			damaged.setHealth(20);
			event.setCancelled(true);
			game.respawnPlayer(damaged, 0);
		}
	}

	public void onPlayerUseItem(PlayerInteractEvent event) {
		if (event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
			if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
				if (event.getPlayer().getItemInHand().getType().equals(Material.BLAZE_ROD)) {
					if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("Rocket Launcher")) {

						event.getPlayer().launchProjectile(Snowball.class);
					}
				}
			}
		}
	}

	public void onHitZombieCoop(EntityDamageByEntityEvent event, Player damager) {
		if (event.getEntity() instanceof Zombie) {

			Zombie zombie = (Zombie) event.getEntity();
			if (zombie.getHealth() - event.getDamage() < 1) {
				ZombiesCoopGame game = (ZombiesCoopGame) Utils.getGameByPlayer(damager);
				game.zombieKilled(zombie, damager.getUniqueId());
			}

		} else if (event.getEntity() instanceof Player) {
			Player damaged = (Player) event.getEntity();
			ZombiesCoopGame game = (ZombiesCoopGame) Utils.getGameByPlayer(damaged);
			event.setCancelled(true);
			if (game.getInjured().contains(damaged.getUniqueId())) {
				if (damager.getItemInHand().getType().equals(Material.STICK)) {
					if (damager.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("Healing Rod")) {
						game.revivePlayer(damaged, damager);
						damager.setItemInHand(new ItemStack(Material.AIR));
					}
				}
			} else {
				damager.sendMessage(ChatColor.RED + "Don't attack other survivors!");
				return;
			}
		}
	}

	@EventHandler
	public void onGrenadeLaunch(final PlayerEggThrowEvent event) {

		if (Utils.getPlayerMap().containsKey(event.getEgg().getShooter())) {

			event.getEgg().setBounce(true);
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				@Override
				public void run() {
					event.getEgg().getWorld().createExplosion(event.getEgg().getLocation(), 3F);
				}
			}, (3 * 20));
		}

	}

	@EventHandler
	public void onLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity().getType().equals(EntityType.SNOWBALL)) {
			Snowball snowball = (Snowball) event.getEntity();
			if (snowball.getShooter() instanceof Player) {
				Player shooter = (Player) snowball.getShooter();

				if (shooter.getItemInHand().getType().equals(Material.BLAZE_ROD)) {
					if (shooter.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("Rocket Launcher")) {
						snowball.setBounce(false);
						snowball.setFireTicks(Integer.MAX_VALUE);
						snowball.playEffect(EntityEffect.WOLF_SMOKE);

					}
				}
			}
		}
	}

	@EventHandler
	public void onRocketHit(ProjectileHitEvent event) {
		if (event.getEntity().getType().equals(EntityType.SNOWBALL)) {
			Snowball snowball = (Snowball) event.getEntity();
			if (snowball.getShooter() instanceof Player) {
				Player shooter = (Player) snowball.getShooter();
				if (Utils.getPlayerMap().containsKey(shooter)) {
					if (shooter.getItemInHand().getType().equals(Material.BLAZE_ROD) && shooter.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("Rocket Launcher")) {
						snowball.getWorld().createExplosion(snowball.getLocation(), 15F);
						snowball.remove();
					}
				}
			}
		}
	}

	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player damaged = (Player) event.getEntity();
			if (Utils.getPlayerMap().containsKey(damaged)) {

				if (Utils.getGameByPlayer(damaged).isInvincible(damaged.getUniqueId())) {
					event.setCancelled(true);
					return;
				}
				if (damaged.getHealth() - event.getDamage() < 1) {

					if (event.getCause().equals(DamageCause.BLOCK_EXPLOSION) || event.getCause().equals(DamageCause.FIRE) || event.getCause().equals(DamageCause.DROWNING) || event.getCause().equals(DamageCause.FALL) || event.getCause().equals(DamageCause.VOID) || event.getCause().equals(DamageCause.SUFFOCATION) || event.getCause().equals(DamageCause.STARVATION) || event.getCause().equals(DamageCause.FIRE_TICK)) {
						Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).respawnPlayer(damaged, Utils.getGameByPlayer(damaged).getRespawnTime());

						event.setCancelled(true);
						damaged.setHealth(20);

						damaged.setFireTicks(0);
						if (PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) {
							// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GREEN
							// + damaged.getName() + " has used second wind!");
							PerksHandler.resetPerk(damaged, Perk.SECOND_WIND);
							return;
						}
						if (Utils.getGameByPlayer(damaged).getType().isTeam()) {
							RedBlueTeamGame game = (RedBlueTeamGame) Utils.getGameByPlayer(damaged);

							game.playerDiedNoKiller(damaged.getUniqueId());
							pke = new PVPPlayerKilledEvent(null, damaged, game, event.getCause(), false, null);
							plugin.getServer().getPluginManager().callEvent(pke);
							return;
						}

					} else if (event.getCause().equals(DamageCause.SUICIDE)) {
						event.setCancelled(true);
						Utils.kickPlayer(damaged.getUniqueId());

					}

				}
			}
		}
	}

	// trimmed some fat but still needs some weight loss. 220 lines for a method
	// is way too
	// many
	public void onHitTeamGame(EntityDamageByEntityEvent event, RedBlueTeamGame game) {
		if (event.getEntity() instanceof Player) {
			Player damaged = (Player) event.getEntity();
			if (Utils.getPlayerMap().containsKey(damaged)) {
				if (Utils.getGameByPlayer(damaged).isInvincible(damaged.getUniqueId())) {
					event.setCancelled(true);
					return;
				}
				if (event.getDamager() instanceof Player) {
					Player damager = (Player) event.getDamager();

					if (Utils.getPlayerMap().containsKey(damager)) {

						if (Utils.waitingToSpawn().contains(damaged.getUniqueId()) || Utils.waitingToSpawn().contains(damager.getUniqueId())) {
							event.setCancelled(true);
							return;
						}

						if (game.getTanking().contains(damaged.getUniqueId())) {
							event.setCancelled(true);
							return;
						}
						if (damager.getInventory().getItemInHand().isSimilar(new ItemStack(Material.STONE_SWORD)) || damager.getInventory().getItemInHand().isSimilar(new ItemStack(Material.WOOD_SWORD)) || damager.getInventory().getItemInHand().isSimilar(new ItemStack(Material.IRON_SWORD)) || damager.getInventory().getItemInHand().isSimilar(new ItemStack(Material.GOLD_SWORD)) || damager.getInventory().getItemInHand().isSimilar(new ItemStack(Material.DIAMOND_SWORD))) {
							event.setDamage((event.getDamage() * PlayerXPHandler.getClass(damager.getUniqueId()).getAttributes().getDamageSwordMult()) + PlayerXPHandler.getClass(damager.getUniqueId()).getAttributes().getDamageSwordBoost());
							event.setDamage(event.getDamage() * PlayerXPHandler.getClass(damaged.getUniqueId()).getAttributes().damageTakeSwordMult);
						}
						event.setDamage((event.getDamage() * PlayerXPHandler.getClass(damager.getUniqueId()).getAttributes().getDamageMult()) + PlayerXPHandler.getClass(damager.getUniqueId()).getAttributes().getDamageBoost());
						event.setDamage(event.getDamage() * PlayerXPHandler.getClass(damaged.getUniqueId()).getAttributes().damageTakeMult);

						if (Utils.getGameByPlayer(damager).getDisguised().contains(damager.getUniqueId())) {
							Utils.getGameByPlayer(damager).getDisguised().remove(damager.getUniqueId());
							game.setPlayerArmor(damager);
							event.setDamage(event.getDamage() + 20D);
							Utils.getTagged().add(damager.getUniqueId());
							final UUID tagged = damager.getUniqueId();
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

								@Override
								public void run() {
									Utils.getTagged().remove(tagged);

								}

							}, 160L);

						}

						if (game.getDisguised().contains(damaged.getUniqueId())) {
							Utils.getGameByPlayer(damager).getDisguised().remove(damager.getUniqueId());
							game.setPlayerArmor(damaged);
							event.setDamage(event.getDamage() + 8D);
							Utils.getTagged().add(damaged.getUniqueId());
							final UUID tagged = damaged.getUniqueId();
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

								@Override
								public void run() {
									Utils.getTagged().remove(tagged);

								}

							}, 160L);

						}
						event.setCancelled(false);
						if (game.isOnSameTeam(damager, damaged)) {

							event.setCancelled(true);
							damager.sendMessage(ChatColor.GOLD + "You may not hurt teammates");
						} else if (damaged.getHealth() - event.getDamage() < 1) {

							event.setCancelled(true);
							damaged.setHealth(20);
							if (PerksHandler.hasPerk(damaged, Perk.SHARE_PAIN)) {
								PerksHandler.resetPerk(damaged, Perk.SHARE_PAIN);
								damager.damage(event.getDamage(), damaged);
							}
							if (PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) {
								PerksHandler.resetPerk(damaged, Perk.SECOND_WIND);
								return;
							}
							pke = new PVPPlayerKilledEvent(damager, damaged, Utils.getGameByPlayer(damager), DamageCause.ENTITY_ATTACK, false, null);
							plugin.getServer().getPluginManager().callEvent(pke);

							respawnTeamPlayer(damaged, damager, game);
							return;

						} else if (PerksHandler.hasPerk(damager, Perk.INSTA_KILL)) {
							PerksHandler.resetPerk(damager, Perk.INSTA_KILL);
							event.setCancelled(true);
							damaged.setHealth(20);
							pke = new PVPPlayerKilledEvent(damager, damaged, Utils.getGameByPlayer(damager), DamageCause.ENTITY_ATTACK, true, Perk.INSTA_KILL);
							if (!PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) plugin.getServer().getPluginManager().callEvent(pke);

							if (PerksHandler.hasPerk(damaged, Perk.SHARE_PAIN)) {
								PerksHandler.resetPerk(damaged, Perk.SHARE_PAIN);
								damager.damage(event.getDamage(), damaged);

								// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GOLD
								// + damaged.getName() +
								// " has used share the pain!");
							}
							if (PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) {
								// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GREEN
								// + damaged.getName() +
								// " has used second wind!");
								PerksHandler.resetPerk(damaged, Perk.SECOND_WIND);
								return;
							}
							respawnTeamPlayer(damaged, damager, game);
							return;

						}

					}
				} else {

					event.setCancelled(false);
					if (!event.getCause().equals(DamageCause.PROJECTILE) && !event.getCause().equals(DamageCause.BLOCK_EXPLOSION) && !event.getCause().equals(DamageCause.VOID) && !event.getCause().equals(DamageCause.CONTACT)) {

						Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).respawnPlayer(damaged, game.getRespawnTime());
						game.getTeam(damaged.getUniqueId()).addSuicide(damaged);

					} else if (event.getCause().equals(DamageCause.PROJECTILE)) {

						if (PlayerXPHandler.getClass(damaged.getUniqueId()).getAttributes().hasAura()) {
							if (game.getTanking().contains(damaged.getUniqueId())) {
								event.setCancelled(true);
								return;
							}
							event.setDamage(event.getDamage() / 1.5);
						}

						Arrow arrow = (Arrow) event.getDamager();
						if (arrow.getShooter() instanceof Player) {
							Player shooter = (Player) arrow.getShooter();

							event.setDamage((event.getDamage() * PlayerXPHandler.getClass(shooter.getUniqueId()).getAttributes().getDamageBowMult()) + PlayerXPHandler.getClass(shooter.getUniqueId()).getAttributes().getDamageBowBoost());

							event.setDamage(event.getDamage() * PlayerXPHandler.getClass(damaged.getUniqueId()).getAttributes().damageTakeBowMult);
							event.setDamage((event.getDamage() * PlayerXPHandler.getClass(shooter.getUniqueId()).getAttributes().getDamageMult()) + PlayerXPHandler.getClass(shooter.getUniqueId()).getAttributes().getDamageBoost());
							event.setDamage(event.getDamage() * PlayerXPHandler.getClass(damaged.getUniqueId()).getAttributes().damageTakeMult);

							if (game.isOnSameTeam(shooter, damaged)) {
								event.setCancelled(true);
								shooter.sendMessage(ChatColor.GOLD + "You may not hurt teammates");
							} else {
								if (damaged.getHealth() - event.getDamage() < 1) {
									event.setCancelled(true);
									damaged.setHealth(20);
									if (PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) {
										// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GREEN
										// + damaged.getName() +
										// " has used second wind!");
										PerksHandler.resetPerk(damaged, Perk.SECOND_WIND);
										return;
									}
									if (PerksHandler.hasPerk(damaged, Perk.SHARE_PAIN)) {
										PerksHandler.resetPerk(damaged, Perk.SHARE_PAIN);
										shooter.damage(event.getDamage(), damaged);

										// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GOLD
										// + damaged.getName() +
										// " has used share the pain!");

									}
									pke = new PVPPlayerKilledEvent(shooter, damaged, Utils.getGameByPlayer(shooter), DamageCause.ENTITY_ATTACK, false, null);
									plugin.getServer().getPluginManager().callEvent(pke);
									respawnTeamPlayer(damaged, shooter, game);
									return;

								} else if (PerksHandler.hasPerk(shooter, Perk.INSTA_KILL)) {
									event.setCancelled(true);
									damaged.setHealth(20);
									pke = new PVPPlayerKilledEvent(shooter, damaged, Utils.getGameByPlayer(shooter), DamageCause.ENTITY_ATTACK, true, Perk.INSTA_KILL);
									if (!PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) plugin.getServer().getPluginManager().callEvent(pke);

									PerksHandler.resetPerk(shooter, Perk.INSTA_KILL);
									if (PerksHandler.hasPerk(damaged, Perk.SECOND_WIND)) {
										// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GREEN
										// + damaged.getName() +
										// " has used second wind!");
										PerksHandler.resetPerk(damaged, Perk.SECOND_WIND);
										return;
									}
									if (PerksHandler.hasPerk(damaged, Perk.SHARE_PAIN)) {
										PerksHandler.resetPerk(damaged, Perk.SHARE_PAIN);
										shooter.damage(event.getDamage(), damaged);

										// Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).broadcast(ChatColor.GOLD
										// + damaged.getName() +
										// " has used share the pain!");

									}
									pke = new PVPPlayerKilledEvent(shooter, damaged, Utils.getGameByPlayer(shooter), DamageCause.ENTITY_ATTACK, false, null);
									plugin.getServer().getPluginManager().callEvent(pke);
									respawnTeamPlayer(damaged, shooter, game);
									return;

								}
							}

						}

					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {

		if (!PlayerXPHandler.hasPlayer(event.getPlayer().getUniqueId())) {
			PlayerXPHandler.config.getConfig().set(event.getPlayer().getUniqueId().toString() + ".xp", 0);
			PlayerXPHandler.config.getConfig().set(event.getPlayer().getUniqueId().toString() + ".level", 1);
			Utils.getGlobalCoins().put(event.getPlayer(), 0);

			PlayerXPHandler.setClass(event.getPlayer().getUniqueId(), PlayerXPHandler.getClasses().get("Soldier"));
			PlayerXPHandler.config.getConfig().set(event.getPlayer().getUniqueId().toString() + ".coins", 0);
			PlayerXPHandler.config.getConfig().set(event.getPlayer().getUniqueId().toString() + ".perkPoints", 0);
			PlayerXPHandler.setPrestige(event.getPlayer().getUniqueId(), 0);
			PlayerXPHandler.config.saveConfig();

		} else {
			Utils.getGlobalCoins().put(event.getPlayer(), PlayerXPHandler.config.getConfig().getInt(event.getPlayer().getUniqueId() + ".coins"));
		}

		//all this should be moved to messaging
		if (messagingConfig.getConfig().getString(event.getPlayer().getUniqueId().toString()) == null) {
			messagingConfig.getConfig().set(event.getPlayer().getUniqueId().toString(), "");
			messagingConfig.saveConfig();
		} else {
			List<String> list = messagingConfig.getConfig().getStringList(event.getPlayer().getUniqueId().toString());

			for (String message : list) {
				Messaging.notify(event.getPlayer(), message);
				// list.remove(message);
			}
			list.clear();
			messagingConfig.getConfig().set(event.getPlayer().getUniqueId().toString(), list);
			messagingConfig.saveConfig();
		}
	}

	// why is this here? it is pointless and won't work
	@EventHandler
	public void onPlayerDie(PlayerDeathEvent event) {
		if (event.getEntityType() == EntityType.PLAYER) {
			Player killed = event.getEntity();
			if (Utils.getPlayerMap().containsKey(killed)) {

				Utils.getGameMap().get(Utils.getPlayerMap().get(killed)).respawnPlayer(killed, 0);

			}
		}

	}

	@EventHandler
	public void onPlayerDisconnect(PlayerQuitEvent event) {
		if (Utils.getPlayerMap().containsKey(event.getPlayer())) {
			System.out.println("kicking player " + event.getPlayer().getName() + " from the pvp game they were in");
			Utils.kickPlayer(event.getPlayer().getUniqueId());
			Messaging.notifyGoingOffline(event.getPlayer(), ChatColor.RED + "You have been kicked from the PVP game you were in!");
		}
	}

	// why am I removing them regardless if they're in there or not? <<find out
	// if this is less efficient << I don't believe
	// so I believe it would take the same amount of time as checking if they
	// were in it first
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		Utils.getInStoreList().remove(event.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onInventoryInteract(InventoryClickEvent event) {

		if (event.getInventory().getHolder() instanceof Chest) {
			Chest chest = (Chest) event.getInventory().getHolder();

			if (Utils.getInStoreList().contains(event.getView().getPlayer().getUniqueId())) {

				// Utils.getInStoreList().remove(event.getView().getPlayer().getName());

				event.getInventory().setContents(ShopInventoryHandler.getDefaultInventory());

				if (event.getSlotType().equals(SlotType.CONTAINER)) {
					if (ShopInventoryHandler.getDefaultInventoryCostMap().containsKey(event.getSlot())) {
						if (!(ShopInventoryHandler.getDefaultInventoryCostMap().get(event.getSlot()) > Utils.getGameByPlayer((Player) event.getView().getPlayer()).getCoins().get(event.getView().getPlayer()))) {
							event.getInventory().setContents(ShopInventoryHandler.getDefaultInventory());
							ItemMeta meta = event.getCurrentItem().getItemMeta();
							meta.setLore(new ArrayList<String>());
							event.getCurrentItem().setItemMeta(meta);
							Utils.getGameByPlayer((Player) event.getView().getPlayer()).getCoins().put((Player) event.getView().getPlayer(), Utils.getGameMap().get(Utils.getPlayerMap().get(event.getView().getPlayer())).getCoins().get(event.getView().getPlayer()) - ShopInventoryHandler.getDefaultInventoryCostMap().get(event.getSlot()));
							Utils.getGameByPlayer((Player) event.getView().getPlayer()).updateCoinBoard(event.getView().getPlayer().getUniqueId());
							chest.update();

						} else {
							event.setCancelled(true);
							((Player) event.getView().getPlayer()).sendMessage(ChatColor.GOLD + "Not enough coins for that");
						}
					}
				}
			}
		} else if (event.getInventory().getHolder() instanceof Player) {
			if (Utils.getPlayerMap().containsKey(event.getView().getPlayer())) {
				if (event.getSlotType().equals(SlotType.ARMOR)) {
					if (event.getCurrentItem().getType().equals(Material.WOOL)) {
						event.setCancelled(true);
					}
				}
			}
		}
	}

	/**
	 * This is used specifically by team games to respawn a player after death.
	 * The multithreading here is experimental as of version 1.5.34_79_A1 and is
	 * expected to be considered experimental until next minor build or beta
	 * build
	 */
	public void respawnTeamPlayer(final Player damaged, final Player damager, final RedBlueTeamGame game) {

		Utils.getGameMap().get(Utils.getPlayerMap().get(damaged)).respawnPlayer(damaged, Utils.getGameByPlayer(damaged).getRespawnTime());

		game.getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@Override
			public void run() {
				game.getTeam(damager.getUniqueId()).addKill(damager, damaged);

			}

		}, 1L));

		return;

	}

}

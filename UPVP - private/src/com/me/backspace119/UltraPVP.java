package com.me.backspace119;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.me.backspace119.events.BlockEventHandler;
import com.me.backspace119.events.FactionEventHandler;
import com.me.backspace119.events.PlayerEventHandler;
import com.me.backspace119.games.Game;
import com.me.backspace119.utils.Messaging;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.CommandHandler;
import com.me.backspace119.utils.handlers.ConfigHandler;
import com.me.backspace119.utils.handlers.ShopInventoryHandler;
import com.me.backspace119.utils.handlers.factions.FactionXPHandler;
import com.me.backspace119.utils.handlers.player.PerksHandler;
import com.me.backspace119.utils.handlers.player.PlayerGearHandler;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Coming Soon: A fitting description and legalese for those who want to use my
 * code
 * 
 * 
 * @author backspace119
 * 
 */
public class UltraPVP extends JavaPlugin {

	public final Logger logger = Logger.getLogger("Minecraft");
	public static UltraPVP plugin;
	public static Economy econ = null;
	public static boolean hasEconomy;
	public static boolean hasFactions;
	public static boolean perksEnabled;
	public static boolean classesEnabled;
	public static ConfigHandler helpFile;

	@Override
	public void onEnable() {
		logger.info("[UltraPVP] UltraPVP v. " + getDescription().getVersion() + " is enabling...");
		plugin = this;

		// both of these are used later in the program for reference
		// this allows a soft depends for Vault and Factions for servers
		// who may not want these plugins
		hasEconomy = setupEconomy();
		hasFactions = setupFactions();

		helpFile = new ConfigHandler(plugin, "help.yml", false);
		ConfigHandler mainConfig = new ConfigHandler(plugin, "upvp.yml", true);
		perksEnabled = mainConfig.getConfig().getBoolean("perksEnabled");
		classesEnabled = mainConfig.getConfig().getBoolean("classesEnabled");
		CommandHandler executor = new CommandHandler(plugin, mainConfig);
		// register command listeners
		ConfigHandler messagingConfig = new ConfigHandler(plugin, "messaging.yml", true);
		getCommand("upvp").setExecutor(executor);
		new Messaging(messagingConfig);
		new FactionXPHandler(new ConfigHandler(plugin, "factionLevels.yml", true), plugin);
		new ShopInventoryHandler(plugin, logger, new ConfigHandler(plugin, "chests.yml", true));
		new PlayerGearHandler(new ConfigHandler(plugin, "gear.yml", true), plugin);
		new PlayerXPHandler(new ConfigHandler(plugin, "levels.yml", true), plugin, mainConfig.getConfig().getInt("difficulty"), new ConfigHandler(this, "classes.yml", true));
		new PerksHandler(plugin);
		Utils.reloadClasses();
		// this simply passes some data to the static methods in Utils.class for
		// later use with things that belong there
		new Utils(plugin, new ConfigHandler(plugin, "games.yml", true), logger, ShopInventoryHandler.getDefaultInventory(), new ConfigHandler(this, "games.yml", true));
		// The config isn't really used...and this isn't handled by my custom
		// config handler, so it has to be checked here
		File file = new File(plugin.getDataFolder().getAbsolutePath() + "/config.yml");
		if (!file.exists()) {
			logger.warning("[UltraPVP] CONFIG FILE DOESN'T EXIST! Had to save default config");
			saveConfig();
		}
		plugin.saveResource("help.yml", true);
		// registering all of my wonderful events (THEY NEED TO BE CLEANED UP)
		getServer().getPluginManager().registerEvents(new BlockEventHandler(), this);
		getServer().getPluginManager().registerEvents(new PlayerEventHandler(messagingConfig, plugin), this);
		if (hasFactions) {
			getServer().getPluginManager().registerEvents(new FactionEventHandler(), this);
		}
		// for people who install this off of a reload
		// we'll check to see if people are already here
		// and not loaded into the system
		// if so we'll go ahead and give them some default stats
		for (Player player : getServer().getOnlinePlayers()) {

			if (!PlayerXPHandler.hasPlayer(player.getUniqueId())) {
				PlayerXPHandler.config.getConfig().set(player.getName() + ".xp", 0);
				PlayerXPHandler.config.getConfig().set(player.getName() + ".level", 1);
				PlayerXPHandler.config.getConfig().set(player.getName() + ".perkPoints", 0);
				PlayerXPHandler.config.getConfig().set(player.getName() + ".perks", new ArrayList<String>());
				Utils.getGlobalCoins().put(player, 0);

				PlayerXPHandler.config.getConfig().set(player + ".coins", 0);

			} else {
				Utils.getGlobalCoins().put(player, PlayerXPHandler.config.getConfig().getInt(player.getName() + ".coins"));
			}

		}
		PlayerXPHandler.save();
		// this reloads all custom (which is all games included
		// on a server at this point) from the games.yml
		// tested and proven to work and work well
		Utils.reloadGames();
		logger.info("[UltraPVP] Enabled");

	}

	/**
	 * checks if the Vault plugin is installed. If it is it will return true and
	 * the global Economy variable will be set to true
	 * 
	 * @return true if Vault exists
	 */
	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			logger.severe("VAULT NOT FOUND!");
			return false;
		}

		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (rsp == null) {
			logger.severe("REGISTERED SERVICE PROVIDER DID NOT FIND VAULT!");
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	/**
	 * checks if the Factions plugin is installed. If it is, it will return true
	 * and the global factions variable will be set to true
	 * 
	 * @return true if Factions exists
	 */
	private boolean setupFactions() {
		if (getServer().getPluginManager().getPlugin("Factions") == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void onDisable() {
		PlayerXPHandler.save();
		FactionXPHandler.save();
		for (Game g : Utils.getGameMap().values()) {
			for (Player p : g.getPlayers()) {
				Utils.kickPlayer(p.getUniqueId());
			}

		}
	}
}

package com.me.backspace119.factories;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.me.backspace119.UltraPVP;
import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.games.ControlPointGame;
import com.me.backspace119.games.Game;
import com.me.backspace119.games.RedBlueTeamGame;
import com.me.backspace119.games.ZombiesCoopGame;
import com.me.backspace119.utils.Utils;

public class GameFactory {

	Player creator;
	World world;
	boolean isTeam;

	public GameFactory(Player creator, World world, String name, GameTypes type, boolean isTeam) {
		this.world = world;
		this.creator = creator;
		this.type = type;
		this.name = name;
		this.isTeam = isTeam;
		Utils.newGameFactory(creator, this);

	}

	public GameTypes getType() {
		return type;
	}

	public boolean isTeamGame() {
		return isTeam;
	}

	String name;
	JavaPlugin plugin = UltraPVP.plugin;
	Location redl;
	Location bluel;
	// Vector mapLocation;
	// ItemStack[] inventory;
	int winKills;
	List<Block> chests = new ArrayList<Block>();
	int ticksInvincible;
	int difficulty;
	// ItemStack[] redArmor;
	// ItemStack[] blueArmor;
	GameTypes type;
	List<Block> refrencedBlocks_1 = new ArrayList<Block>();
	List<Block> refrencedBlocks_2 = new ArrayList<Block>();
	List<Location> spectatorLocations = new ArrayList<Location>();
	List<Location> zombieSpawns = new ArrayList<Location>();
	List<Location> nonTeamSpawns = new ArrayList<Location>();

	public void stepSpawnLocations(Location l) {
		nonTeamSpawns.add(l);
	}

	public void stepRedLocation(Location location) {
		redl = location;
	}

	public void stepBlueLocation(Location location) {
		bluel = location;
	}

	public void stepWinkills(int winKills) {
		this.winKills = winKills;
	}

	public void stepAddChest(Block chest) {
		if (chest != null) chests.add(chest);

	}

	public void stepSecondsInvincible(int secondsInvincible) {
		ticksInvincible = secondsInvincible * 20;
	}

	public void stepRedControlPoint(Block block) {
		if (block != null) refrencedBlocks_1.add(block);
	}

	public void stepBlueControlPoint(Block block) {
		if (block != null) refrencedBlocks_2.add(block);
	}

	public void stepZombie(Location l) {
		zombieSpawns.add(l);
	}

	public void stepSpectators(Location l) {
		spectatorLocations.add(l);
	}

	Chest medChest;

	public void stepZombieMedChest(Chest c) {
		medChest = c;
	}

	public Game createGame() {
		ItemStack[] redArmor = new ItemStack[4];
		ItemStack[] blueArmor = new ItemStack[4];

		redArmor[3] = new ItemStack(Material.WOOL, 1, (byte) 14);
		// redArmor[3].setData(new Wool(DyeColor.RED));
		blueArmor[3] = new ItemStack(Material.WOOL, 1, (byte) 11);
		blueArmor[0] = new ItemStack(Material.LEATHER_BOOTS);
		blueArmor[1] = new ItemStack(Material.LEATHER_LEGGINGS);
		blueArmor[2] = new ItemStack(Material.LEATHER_CHESTPLATE);
		redArmor[0] = new ItemStack(Material.LEATHER_BOOTS);
		redArmor[1] = new ItemStack(Material.LEATHER_LEGGINGS);
		redArmor[2] = new ItemStack(Material.LEATHER_CHESTPLATE);
		// blueArmor[3].setData(new Wool(DyeColor.BLUE));
		Utils.removeGameFactory(creator);
		Utils.getGamesConfig().set(name + ".world", world.getName());
		if (isTeam) {
			Utils.getGamesConfig().set(name + ".redl." + "x", redl.getBlockX());
			Utils.getGamesConfig().set(name + ".redl." + "y", redl.getBlockY());
			Utils.getGamesConfig().set(name + ".redl." + "z", redl.getBlockZ());
			Utils.getGamesConfig().set(name + ".bluel." + "x", bluel.getBlockX());
			Utils.getGamesConfig().set(name + ".bluel." + "y", bluel.getBlockY());
			Utils.getGamesConfig().set(name + ".bluel." + "z", bluel.getBlockZ());
		} else {
			int i = 0;
			for (Location l : nonTeamSpawns) {
				Utils.getGamesConfig().set(name + ".spawn." + i + ".x", l.getBlockX());
				Utils.getGamesConfig().set(name + ".spawn." + i + ".y", l.getBlockY());
				Utils.getGamesConfig().set(name + ".spawn." + i + ".z", l.getBlockZ());
				i++;
			}
		}
		if (getType().equals(GameTypes.COOP_ZOMBIES)) {
			int i = 0;
			for (Location l : zombieSpawns) {
				Utils.getGamesConfig().set(name + ".zspawn." + i + ".x", l.getBlockX());
				Utils.getGamesConfig().set(name + ".zspawn." + i + ".y", l.getBlockY());
				Utils.getGamesConfig().set(name + ".zspawn." + i + ".z", l.getBlockZ());
				i++;
			}
			Utils.getGamesConfig().set(name + ".difficulty", difficulty);
			Utils.getGamesConfig().set(name + ".mchest." + ".x", medChest.getX());
			Utils.getGamesConfig().set(name + ".mchest." + ".y", medChest.getY());
			Utils.getGamesConfig().set(name + ".mchest." + ".z", medChest.getZ());
		}
		Utils.getGamesConfig().set(name + ".winKills", winKills);
		int i = 0;
		for (Block b : chests) {
			Utils.getGamesConfig().set(name + ".chests." + i + ".x", b.getX());
			Utils.getGamesConfig().set(name + ".chests." + i + ".y", b.getY());
			Utils.getGamesConfig().set(name + ".chests." + i + ".z", b.getZ());

			i++;
		}
		i = 0;
		for (Location l : spectatorLocations) {
			Utils.getGamesConfig().set(name + ".spectator." + i + ".x", l.getBlockX());
			Utils.getGamesConfig().set(name + ".spectator." + i + ".y", l.getBlockY());
			Utils.getGamesConfig().set(name + ".spectator." + i + ".z", l.getBlockZ());
			i++;
		}
		if (getType().equals(GameTypes.CONTROLPOINT)) {
			i = 0;
			for (Block b : refrencedBlocks_1) {
				Utils.getGamesConfig().set(name + ".r1." + i + ".x", b.getX());
				Utils.getGamesConfig().set(name + ".r1." + i + ".y", b.getY());
				Utils.getGamesConfig().set(name + ".r1." + i + ".z", b.getZ());

				i++;
			}

			i = 0;
			for (Block b : refrencedBlocks_2) {
				Utils.getGamesConfig().set(name + ".r2." + i + ".x", b.getX());
				Utils.getGamesConfig().set(name + ".r2." + i + ".y", b.getY());
				Utils.getGamesConfig().set(name + ".r2." + i + ".z", b.getZ());

				i++;
			}
		}

		Utils.getGamesConfig().set(name + ".ticksInvincible", ticksInvincible);
		Utils.getGamesConfig().set(name + ".type", type.name());

		Utils.saveGamesConfig();

		if (type.equals(GameTypes.CONTROLPOINT)) {
			return new ControlPointGame(name, plugin, redl, bluel, null, winKills, chests, ticksInvincible, redArmor, blueArmor, type, refrencedBlocks_1, refrencedBlocks_2, world, true, world.getName());

		} else if (type.equals(GameTypes.TEAM_PVP)) {
			return new RedBlueTeamGame(name, plugin, redl, bluel, null, winKills, chests, ticksInvincible, redArmor, blueArmor, type, world, true, world.getName());

		} else if (type.equals(GameTypes.COOP_ZOMBIES)) {
			return new ZombiesCoopGame(name, plugin, spectatorLocations, winKills, chests, ticksInvincible, type, world, true, world.getName(), nonTeamSpawns, zombieSpawns, difficulty, medChest);
		} else {
			return null;
		}
	}

}

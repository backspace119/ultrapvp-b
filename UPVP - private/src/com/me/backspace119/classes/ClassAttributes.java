
package com.me.backspace119.classes;

public class ClassAttributes {

	public int damageBoost;

	public int getDamageBoost() {
		return damageBoost;
	}

	public double getDamageMult() {
		return damageMult;
	}

	public int getDamageSwordBoost() {
		return damageSwordBoost;
	}

	public double getDamageSwordMult() {
		return damageSwordMult;
	}

	public int getDamageBowBoost() {
		return damageBowBoost;
	}

	public double getDamageBowMult() {
		return damageBowMult;
	}

	public int getHealthBoost() {
		return healthBoost;
	}

	public int getHealthMult() {
		return healthMult;
	}

	public boolean hasAura() {
		return aura;
	}

	public boolean hasChangeCostume() {
		return changeCostume;
	}

	public int getJumpBoost() {
		return jumpBoost;
	}

	public double getSpeedBoost() {
		return speedBoost;
	}

	public boolean hasInvisible() {
		return invisible;
	}

	public double damageMult;

	public void setDamageBoost(int damageBoost) {
		this.damageBoost = damageBoost;
	}

	public void setDamageMult(double damageMult) {
		this.damageMult = damageMult;
	}

	public void setDamageSwordBoost(int damageSwordBoost) {
		this.damageSwordBoost = damageSwordBoost;
	}

	public void setDamageSwordMult(double damageSwordMult) {
		this.damageSwordMult = damageSwordMult;
	}

	public void setDamageBowBoost(int damageBowBoost) {
		this.damageBowBoost = damageBowBoost;
	}

	public void setDamageBowMult(double damageBowMult) {
		this.damageBowMult = damageBowMult;
	}

	public void setHealthBoost(int healthBoost) {
		this.healthBoost = healthBoost;
	}

	public void setHealthMult(int healthMult) {
		this.healthMult = healthMult;
	}

	public void setAura(boolean aura) {
		this.aura = aura;
	}

	public void setChangeCostume(boolean changeCostume) {
		this.changeCostume = changeCostume;
	}

	public void setJumpBoost(int jumpBoost) {
		this.jumpBoost = jumpBoost;
	}

	public void setSpeedBoost(double speedBoost) {
		this.speedBoost = speedBoost;
	}

	public void setInvisible(boolean invisible) {
		this.invisible = invisible;
	}

	public void setDamageTakeBowMult(double damageTakeBowMult) {
		this.damageTakeBowMult = damageTakeBowMult;
	}

	public void setDamageTakeMult(double damageTakeMult) {
		this.damageTakeMult = damageTakeMult;
	}

	public void setDamageTakeSwordMult(double damageTakeSwordMult) {
		this.damageTakeSwordMult = damageTakeSwordMult;
	}

	public int damageSwordBoost;
	public double damageSwordMult;
	public int damageBowBoost;
	public double damageBowMult;
	public int healthBoost;
	public int healthMult;
	public boolean aura;
	public boolean changeCostume;
	public int jumpBoost;
	public double speedBoost;
	public boolean invisible;
	public double damageTakeBowMult;

	public double getDamageTakeBowMult() {
		return damageTakeBowMult;
	}

	public double getDamageTakeMult() {
		return damageTakeMult;
	}

	public double getDamageTakeSwordMult() {
		return damageTakeSwordMult;
	}

	public double damageTakeMult;
	public double damageTakeSwordMult;

	public ClassAttributes(int dB, double dM, int dSB, double dSM, int dBB, double dBM, int hB, int hM, boolean aura, boolean change, int jB, double sB, boolean invis, double dTBM, double dTSM, double dTM) {
		damageBoost = dB;
		damageMult = dM;
		damageSwordBoost = dSB;
		damageSwordMult = dSM;
		damageBowBoost = dBB;
		damageBowMult = dBM;
		healthBoost = hB;
		healthMult = hM;
		this.aura = aura;
		changeCostume = change;
		jumpBoost = jB;
		speedBoost = sB;
		invisible = invis;
		damageTakeBowMult = dTBM;
		damageTakeMult = dTM;
		damageTakeSwordMult = dTSM;
	}

}

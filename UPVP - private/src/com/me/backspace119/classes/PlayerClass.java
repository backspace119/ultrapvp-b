package com.me.backspace119.classes;

import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

public class PlayerClass {

	private String description;
	private final int levelRequired;
	private final String basicName;
	private ClassAttributes classAttributes;

	public PlayerClass(String description, int levelRequired, String basicName, ClassAttributes attributes, boolean save) {
		this.description = description;
		this.levelRequired = levelRequired;
		this.basicName = basicName;
		classAttributes = attributes;
		PlayerXPHandler.addClass(this, basicName);
		if(save)
		{
			save();
		}
	}

	public void save() {
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName, levelRequired);
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".description", description);
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageBoost", classAttributes.getDamageBoost());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageBowBoost", classAttributes.getDamageBowBoost());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageBowMult", classAttributes.getDamageBowMult());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageMult", classAttributes.getDamageMult());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageSwordBoost", classAttributes.getDamageSwordBoost());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageSwordMult", classAttributes.getDamageSwordMult());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageTakeBowMult", classAttributes.getDamageTakeBowMult());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageTakeMult", classAttributes.getDamageTakeMult());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "damageTakeSwordMult", classAttributes.getDamageTakeSwordMult());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "jumpBoost", classAttributes.getJumpBoost());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "hasAura", classAttributes.hasAura());
		PlayerXPHandler.getClassConfigHandler().getConfig().set(basicName + ".attributes." + "hasChangeCostume", classAttributes.hasChangeCostume());
		PlayerXPHandler.getClassConfigHandler().saveConfig();

	}

	public String description() {
		return description;
	}

	public void setDescription(String desc) {
		description = desc;
	}

	public int levelRequired() {
		return levelRequired;
	}

	public String getName() {
		return basicName;
	}

	public ClassAttributes getAttributes() {
		return classAttributes;
	}

	public void setAttributes(ClassAttributes ca) {
		classAttributes = ca;
	}
}

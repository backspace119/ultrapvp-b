package com.me.backspace119.api;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.games.Game;

/**
 * Legalese coming soon!
 * 
 * Now about what this class does:
 * 
 * This class is an Event that is handled by the bukkit events system. Simply
 * make a listener for this event and every time a player is killed inside a
 * UltraPVP game it will throw this event. This is useful if: you are looking to
 * keep overall stats in other places, you want to add an award for killing in
 * game, you have any other need to keep track of when a player kills a player
 * in game, etc.
 * 
 * @author backspace119
 * 
 */
public class PVPPlayerKilledEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private Player killer;
	private Player killed;
	private Game game;
	private DamageCause damageCause;
	private boolean perkUsed;
	private Perk perk;

	/**
	 * Thrown whenever a player is killed in game and both the killer and killed
	 * are known. Might soon add functionality for if the killer isn't known.
	 * (like a TNT event because at the present moment it would seem that
	 * finding who lit the TNT is a bit harder than expected)
	 * 
	 * @param killer
	 * @param killed
	 * @param game
	 * @param damageCause
	 * @param perkUsed
	 * @param perk
	 */
	public PVPPlayerKilledEvent(Player killer, Player killed, Game game, DamageCause damageCause, boolean perkUsed, Perk perk) {
		this.killer = killer;
		this.killed = killed;
		this.game = game;
		this.damageCause = damageCause;
		this.perkUsed = perkUsed;
		this.perk = perk;
	}

	/**
	 * gets the player that did the killing. Good for them.
	 * 
	 * @return Player
	 */
	public Player getKiller() {
		return killer;
	}

	/**
	 * gets the player that was killed in this event. Simple as that.
	 * 
	 * @return Player
	 */
	public Player getKilled() {
		return killed;
	}

	/**
	 * Gets an instance of the game that both players are in. Useful for
	 * controlling score of a team while in game or finding which team a player
	 * was on.
	 * 
	 * @return Game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * gets the bukkit DamageCause that was the cause of death for the killed
	 * player (i.e. melee or projectile or etc.)
	 * 
	 * @return DamageCause
	 */
	public DamageCause getDamageCause() {
		return damageCause;
	}

	/**
	 * tells you whether a perk was used to kill the player or not
	 * 
	 * @return boolean
	 */
	public boolean isPerkUsed() {
		return perkUsed;
	}

	/**
	 * gets which perk was used when killing the player IF a perk was used
	 * 
	 * @return Perk
	 */
	public Perk getPerk() {
		return perk;
	}

	@Override
	public HandlerList getHandlers() {

		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}

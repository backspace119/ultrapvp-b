package com.me.backspace119.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import com.me.backspace119.classes.ClassAttributes;
import com.me.backspace119.classes.PlayerClass;
import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.factories.GameFactory;
import com.me.backspace119.games.ControlPointGame;
import com.me.backspace119.games.RedBlueTeamGame;
import com.me.backspace119.games.Game;
import com.me.backspace119.games.ZombiesCoopGame;
import com.me.backspace119.utils.handlers.ConfigHandler;
import com.me.backspace119.utils.handlers.player.PerksHandler;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

public class Utils {
	private static Map<Player, String> playerMap = new HashMap<Player, String>();
	private static Map<String, Game> gameMap = new HashMap<String, Game>();
	private static Map<Player, Integer> globalKillCoins = new HashMap<Player, Integer>();
	public static Map<UUID, Integer> creatingWorld;
	static JavaPlugin plugin;
	static ConfigHandler games;

	public static Logger logger;
	static Map<Block, Game> signMap = new HashMap<Block, Game>();
	static List<UUID> inStoreList = new ArrayList<UUID>();
	static ItemStack[] shopChestInventory;
	static List<Location> chestList = new ArrayList<Location>();
	static List<UUID> waitingToSpawn;
	static Location waitArea;
	static List<UUID> tagged;
	static Map<String, List<Location>> signs;
	static Map<UUID, GameFactory> gameFactories;
	static Map<UUID, Location> savedPlayerLocations = new HashMap<UUID, Location>();
	static Map<UUID, ItemStack[]> savedPlayerInventories = new HashMap<UUID, ItemStack[]>();
	static Map<UUID, ItemStack[]> savedPlayerArmor = new HashMap<UUID, ItemStack[]>();
	static Map<UUID, String> makingClass = new HashMap<UUID, String>();
	static ConfigHandler gamesConfig;

	public Utils(JavaPlugin plugin, ConfigHandler games, Logger logger, ItemStack[] shopChestInventory, ConfigHandler gamesConfig) {
		Utils.gameFactories = new HashMap<UUID, GameFactory>();
		Utils.plugin = plugin;
		Utils.games = games;
		Utils.gamesConfig = gamesConfig;
		Utils.logger = logger;
		Utils.shopChestInventory = shopChestInventory;
		Utils.inStoreList.clear();
		Utils.waitingToSpawn = new ArrayList<UUID>();
		
		Utils.tagged = new ArrayList<UUID>();
		Utils.signs = new HashMap<String, List<Location>>();
		Utils.creatingWorld = new HashMap<UUID, Integer>();
	}

	public static void saveGamesConfig() {
		gamesConfig.saveConfig();
	}

	public static FileConfiguration getGamesConfig() {
		return gamesConfig.getConfig();
	}

	public static boolean isMakingClass(UUID id) {
		return makingClass.containsKey(id);
	}

	public static String getMakingClass(UUID id) {
		return makingClass.get(id);
	}

	public static void setMakingClass(UUID id, boolean b, String className) {
		if (b) {
			makingClass.put(id, className);
		} else {
			makingClass.remove(id);
		}
	}

	public static List<Location> getSignsForGame(String name) {
		return signs.get(name);
	}

	public static void updateSignsForTeamGame(String name) {
		if (gameMap.get(name).getType().isTeam()) {
			for (Location l : getSignsForGame(name)) {
				Sign sign = (Sign) plugin.getServer().getWorld(l.getWorld().getName()).getBlockAt(l).getState();
				sign.setLine(3, ChatColor.RED + "" + ((RedBlueTeamGame) gameMap.get(name)).getRedPlayers().size() + ChatColor.BLACK + "/" + ChatColor.BLUE + "" + ((RedBlueTeamGame) gameMap.get(name)).getBluePlayers().size());
				sign.update();
			}
		}
	}

	public static GameFactory getGameFactory(Player player) {
		return gameFactories.get(player.getUniqueId());
	}

	public static void newGameFactory(Player player, GameFactory factory) {
		gameFactories.put(player.getUniqueId(), factory);
	}

	public static void setSignsForGame(String name, List<Location> s) {
		signs.put(name, s);
	}

	public static void removeGameFactory(Player player) {
		gameFactories.remove(player.getUniqueId());
	}

	public static List<UUID> getTagged() {
		return tagged;
	}

	public static Game getGameByPlayer(Player player) {
		return gameMap.get(playerMap.get(player));
	}

	public static ItemStack[] getShopChestInventory() {
		return shopChestInventory;
	}

	public static Map<Block, Game> getSignMap() {
		return signMap;
	}

	public static void addToGame(Player player, String game) {
		playerMap.put(player, game);
	}

	public static void removeFromGame(Player player) {
		savedPlayerLocations.remove(player.getUniqueId());
		player.getInventory().setArmorContents(savedPlayerArmor.get(player.getUniqueId()));
		player.getInventory().setContents(savedPlayerInventories.get(player.getUniqueId()));
		savedPlayerInventories.remove(player.getUniqueId());
		savedPlayerArmor.remove(player.getUniqueId());
		playerMap.remove(player);
	}

	public static Map<Player, String> getPlayerMap() {
		return playerMap;
	}

	public static void kickPlayer(UUID id) {

		Game game = gameMap.get(playerMap.get(plugin.getServer().getPlayer(id)));
		RedBlueTeamGame tGame = null;
		if (game.getType().isTeam()) {
			tGame = (RedBlueTeamGame) gameMap.get(playerMap.get(plugin.getServer().getPlayer(id)));
		}

		game.getPlayers().remove(plugin.getServer().getPlayer(id));
		for (PotionEffect e : plugin.getServer().getPlayer(id).getActivePotionEffects()) {
			plugin.getServer().getPlayer(id).removePotionEffect(e.getType());
		}
		if (game.getType().isTeam()) {
			if (tGame.getBluePlayers().contains(plugin.getServer().getPlayer(id))) {
				tGame.getBluePlayers().remove(plugin.getServer().getPlayer(id));
			} else {
				tGame.getRedPlayers().remove(plugin.getServer().getPlayer(id));
			}
			for (Location l : Utils.getSignsForGame(playerMap.get(plugin.getServer().getPlayer(id)))) {
				Sign sign = (Sign) plugin.getServer().getWorld(l.getWorld().getName()).getBlockAt(l).getState(); // <
																													// this
																													// means
																													// the
																													// sign
																													// cannot
																													// reside
																													// in
																													// the
																													// same
																													// world
																													// as
																													// the
																													// game:
																													// duh
				sign.setLine(3, ChatColor.RED + "" + tGame.getRedPlayers().size() + ChatColor.BLACK + "/" + ChatColor.BLUE + "" + tGame.getBluePlayers().size());
				sign.update();
			}
			if (tGame.getRedPlayers().size() < 1 || tGame.getBluePlayers().size() < 1) tGame.teamWonWithoutPlayer();

		}
		gameMap.get(playerMap.get(plugin.getServer().getPlayer(id))).noChat(plugin.getServer().getPlayer(id));

		game.getKickedPlayers().add(id);

		inStoreList.remove(id);

		plugin.getServer().getPlayer(id).getInventory().clear();
		plugin.getServer().getPlayer(id).getInventory().setArmorContents(new ItemStack[4]);
		plugin.getServer().getPlayer(id).teleport(getSpawnLocation(id));
		plugin.getServer().getPlayer(id).setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		plugin.getServer().getPlayer(id).setHealth(20D);
		plugin.getServer().getPlayer(id).setFoodLevel(20);
		plugin.getServer().getPlayer(id).setSaturation(2F);
		plugin.getServer().getPlayer(id).setDisplayName(plugin.getServer().getPlayer(id).getName());
		PerksHandler.removePlayer(plugin.getServer().getPlayer(id));
		Messaging.notify(plugin.getServer().getPlayer(id), ChatColor.RED + "YOU HAVE BEEN KICKED FROM THE PVP GAME YOU WERE IN!");
		removeFromGame(plugin.getServer().getPlayer(id));
	}

	public static void joinGame(Player player, String game) {
		if (!gameMap.get(game).isRestarting()) {
			if (gameMap.get(game).getKickedPlayers().contains(player.getUniqueId())) {
				player.sendMessage(ChatColor.RED + "You have been kicked from this game. Wait for it to restart to join again");
			} else if (playerMap.containsKey(player)) {
				gameMap.get(playerMap.get(player)).respawnPlayer(player, 0);
			} else {
				savedPlayerLocations.put(player.getUniqueId(), player.getLocation());
				savedPlayerInventories.put(player.getUniqueId(), player.getInventory().getContents());
				savedPlayerArmor.put(player.getUniqueId(), player.getInventory().getArmorContents());
				playerMap.put(player, game);
				gameMap.get(game).joinGame(player, "");

			}
		} else {
			player.sendMessage(ChatColor.GOLD + "That game is currently restarting please wait until it finishes to join");
		}
	}

	// what the fuck am I even doing here
	// VVVVVVVV
	int step;

	public static boolean isInCreation(Player player) {
		return creatingWorld.containsKey(player.getUniqueId());
	}

	public static void startedCreation(Player player) {
		creatingWorld.put(player.getUniqueId(), 1);
	}

	public static Game endedCreation(Player player, boolean saveGame) {
		creatingWorld.remove(player.getUniqueId());
		if (saveGame) {
			return gameFactories.get(player.getUniqueId()).createGame();
		}
		return null;
	}

	public static int atStep(Player player) {
		return creatingWorld.get(player.getUniqueId());
	}

	public static void nextStep(Player player) {
		creatingWorld.put(player.getUniqueId(), creatingWorld.get(player.getUniqueId()) + 1);
	}

	public static Location waitArea() {
		return waitArea;
	}

	public static List<Location> getChestList() {
		return chestList;
	}

	public static Map<String, Game> getGameMap() {
		return gameMap;
	}

	public static Map<Player, Integer> getGlobalCoins() {
		return globalKillCoins;
	}

	public static void broadcast() {

	}

	public static void reloadClasses() {
		for (String value : PlayerXPHandler.getClassConfigHandler().getConfig().getKeys(false)) {
			new PlayerClass(PlayerXPHandler.getClassConfigHandler().getConfig().getString(value + ".description"), PlayerXPHandler.getClassConfigHandler().getConfig().getInt(value), value, new ClassAttributes(0, 1.0, 0, 1.0, 0, 1.0, 0, 1, false, false, 0, 0.0, false, 1.0, 1.0, 1.0), false);

			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageBoost(PlayerXPHandler.getClassConfigHandler().getConfig().getInt(value + ".attributes." + "damageBoost"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageBowBoost(PlayerXPHandler.getClassConfigHandler().getConfig().getInt(value + ".attributes." + "damageBowBoost"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageBowMult(PlayerXPHandler.getClassConfigHandler().getConfig().getDouble(value + ".attributes." + "damageBowMult"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageMult(PlayerXPHandler.getClassConfigHandler().getConfig().getDouble(value + ".attributes." + "damageMult"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageSwordBoost(PlayerXPHandler.getClassConfigHandler().getConfig().getInt(value + ".attributes." + "damageSwordBoost"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageSwordMult(PlayerXPHandler.getClassConfigHandler().getConfig().getDouble(value + ".attributes." + "damageSwordMult"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageTakeBowMult(PlayerXPHandler.getClassConfigHandler().getConfig().getDouble(value + ".attributes." + "damageTakeBowMult"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageTakeMult(PlayerXPHandler.getClassConfigHandler().getConfig().getDouble(value + ".attributes." + "damageTakeMult"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setDamageTakeSwordMult(PlayerXPHandler.getClassConfigHandler().getConfig().getDouble(value + ".attributes." + "damageTakeSwordMult"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setJumpBoost(PlayerXPHandler.getClassConfigHandler().getConfig().getInt(value + ".attributes." + "jumpBoost"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setAura(PlayerXPHandler.getClassConfigHandler().getConfig().getBoolean(value + ".attributes." + "hasAura"));
			PlayerXPHandler.getClasses().get(value).getAttributes().setChangeCostume(PlayerXPHandler.getClassConfigHandler().getConfig().getBoolean(value + ".attributes." + "hasChangeCostume"));
			
		}
	}

	public static List<UUID> waitingToSpawn() {
		return waitingToSpawn;
	}

	public static List<UUID> getInStoreList() {
		return inStoreList;
	}

	/**
	 * WORKS AND CLEAN(ish)
	 */
	public static void reloadGames() {
		ItemStack[] redArmor = new ItemStack[4];
		ItemStack[] blueArmor = new ItemStack[4];

		redArmor[3] = new ItemStack(Material.WOOL, 1, (byte) 14);
		//redArmor[3].setData(new Wool(DyeColor.RED));
		blueArmor[3] = new ItemStack(Material.WOOL, 1, (byte) 11);
		blueArmor[0] = new ItemStack(Material.LEATHER_BOOTS);
		blueArmor[1] = new ItemStack(Material.LEATHER_LEGGINGS);
		blueArmor[2] = new ItemStack(Material.LEATHER_CHESTPLATE);
		redArmor[0] = new ItemStack(Material.LEATHER_BOOTS);
		redArmor[1] = new ItemStack(Material.LEATHER_LEGGINGS);
		redArmor[2] = new ItemStack(Material.LEATHER_CHESTPLATE);
		//blueArmor[3].setData(new Wool(DyeColor.BLUE));
		for (String value : getGamesConfig().getKeys(false)) {
			List<Block> r1 = new ArrayList<Block>();
			List<Block> r2 = new ArrayList<Block>();
			List<Block> chests = new ArrayList<Block>();
			List<Location> spawnLocations = new ArrayList<Location>();
			List<Location> spectatorLocations = new ArrayList<Location>();
			List<Location> zombieSpawns = new ArrayList<Location>();
			Chest medChest = null;
			GameTypes type = GameTypes.TEAM_PVP;
			World world = plugin.getServer().getWorld(getGamesConfig().getString(value + ".world"));
			for (GameTypes t : GameTypes.values()) {
				if (getGamesConfig().getString(value + ".type").equalsIgnoreCase(t.name())) {
					type = t;
					break;
				}
			}
			chests = resetChests(value);
			for (String v : getGamesConfig().getConfigurationSection(value + ".spectator").getKeys(false)) {
				int x = Utils.getGamesConfig().getInt(value + ".spectator." + v + ".x");
				int y = Utils.getGamesConfig().getInt(value + ".spectator." + v + ".y");
				int z = Utils.getGamesConfig().getInt(value + ".spectator." + v + ".z");
				spawnLocations.add(new Location(world, x, y, z));
			}

			if (type.equals(GameTypes.CONTROLPOINT)) {
				
				r1 = resetControlPointSet(value + ".r1", value);
				r2 = resetControlPointSet(value + ".r2", value);
				
			}
			if (!type.isTeam()) {
				for (String v : getGamesConfig().getConfigurationSection(value + ".spawn").getKeys(false)) {
					int x = Utils.getGamesConfig().getInt(value + ".spawn." + v + ".x");
					int y = Utils.getGamesConfig().getInt(value + ".spawn." + v + ".y");
					int z = Utils.getGamesConfig().getInt(value + ".spawn." + v + ".z");
					spawnLocations.add(new Location(world, x, y, z));
				}
			}
			if (type.equals(GameTypes.COOP_ZOMBIES)) {
				for (String v : getGamesConfig().getConfigurationSection(value + ".zspawn").getKeys(false)) {
					int x = Utils.getGamesConfig().getInt(value + ".zspawn." + v + ".x");
					int y = Utils.getGamesConfig().getInt(value + ".zspawn." + v + ".y");
					int z = Utils.getGamesConfig().getInt(value + ".zspawn." + v + ".z");
					zombieSpawns.add(new Location(world, x, y, z));
				}
				int x = Utils.getGamesConfig().getInt(value + ".mchest.x");
				int y = Utils.getGamesConfig().getInt(value + ".mchest.y");
				int z = Utils.getGamesConfig().getInt(value + ".mchest.z");
				medChest = (Chest) world.getBlockAt(new Location(world, x, y, z)).getState();

			}

			if (type.equals(GameTypes.CONTROLPOINT)) {
				new ControlPointGame(value, plugin, new Location(plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), getGamesConfig().getInt(value + ".redl.x"), getGamesConfig().getInt(value + ".redl.y"), getGamesConfig().getInt(value + ".redl.z")), new Location(plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), getGamesConfig().getInt(value + ".bluel.x"), getGamesConfig().getInt(value + ".bluel.y"), getGamesConfig().getInt(value + ".bluel.z")), spectatorLocations, getGamesConfig().getInt(value + ".winKills"), chests, getGamesConfig().getInt(value + ".ticksInvincible"), redArmor, blueArmor, type, r1, r2, plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), true, getGamesConfig().getString(value + ".world"));
			} else if (type.isTeam()) {
				new RedBlueTeamGame(value, plugin, new Location(plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), getGamesConfig().getInt(value + ".redl.x"), getGamesConfig().getInt(value + ".redl.y"), getGamesConfig().getInt(value + ".redl.z")), new Location(plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), getGamesConfig().getInt(value + ".bluel.x"), getGamesConfig().getInt(value + ".bluel.y"), getGamesConfig().getInt(value + ".bluel.z")), spectatorLocations, getGamesConfig().getInt(value + ".winKills"), chests, getGamesConfig().getInt(value + ".ticksInvincible"), redArmor, blueArmor, type, plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), true, getGamesConfig().getString(value + ".world"));
				//return new Game(name, plugin, redl, bluel, null, PlayerGearHandler.defaultGear(), winKills, chests, ticksInvincible, redArmor, blueArmor, type, refrencedBlocks_1, refrencedBlocks_2, world, true);
			} else if (type.equals(GameTypes.COOP_ZOMBIES)) {
				new ZombiesCoopGame(value, plugin, spectatorLocations, getGamesConfig().getInt(value + ".winKills"), chests, getGamesConfig().getInt(value + ".ticksInvincible"), type, plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), true, getGamesConfig().getString(value + ".world"), spawnLocations, zombieSpawns, getGamesConfig().getInt(value + ".difficulty"), medChest);
			}
		}
	}

	public static List<Block> resetChests(String value) {
		List<Block> chests = new ArrayList<Block>();
		for (String v2 : getGamesConfig().getConfigurationSection(value + ".chests").getKeys(false)) {

			int x = Utils.getGamesConfig().getInt(value + ".chests." + v2 + ".x");
			int y = Utils.getGamesConfig().getInt(value + ".chests." + v2 + ".y");
			int z = Utils.getGamesConfig().getInt(value + ".chests." + v2 + ".z");
			chests.add(plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")).getBlockAt(new Location(plugin.getServer().getWorld(getGamesConfig().getString(value + ".world")), x, y, z)));
		}
		return chests;
	}

	
	public static List<Block> resetControlPointSet(String value, String name)
	{
		List<Block> r = new ArrayList<Block>();
		for (String v2 : getGamesConfig().getConfigurationSection(value).getKeys(false)) {
			int x = Utils.getGamesConfig().getInt(value + "." + v2 + ".x");
			int y = Utils.getGamesConfig().getInt(value + "." + v2 + ".y");
			int z = Utils.getGamesConfig().getInt(value + "." + v2 + ".z");
			r.add(plugin.getServer().getWorld(getGamesConfig().getString(name + ".world")).getBlockAt(new Location(plugin.getServer().getWorld(getGamesConfig().getString(name + ".world")), x, y, z)));

		}

		return r;
		
	}
	public static Location getSpawnLocation(UUID name) {
		return savedPlayerLocations.get(name);
	}
}

package com.me.backspace119.utils;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.me.backspace119.utils.handlers.ConfigHandler;

public class Messaging {

	static ConfigHandler messagingConfig;

	public Messaging(ConfigHandler messagingConfig) {
		Messaging.messagingConfig = messagingConfig;
	}

	public static void notify(Player player, String message) {
		if (player.isOnline()) {
			player.sendMessage(ChatColor.BLUE + message);
		} else {
			List<String> messages = messagingConfig.getConfig().getStringList(player.getUniqueId().toString());
			messages.add(message);
			messagingConfig.getConfig().set(player.getUniqueId().toString(), messages);
			messagingConfig.saveConfig();
		}
	}

	public static void notifyGoingOffline(Player player, String message) {
		List<String> messages = messagingConfig.getConfig().getStringList(player.getUniqueId().toString());
		messages.add(message);
		messagingConfig.getConfig().set(player.getUniqueId().toString(), messages);
		messagingConfig.saveConfig();
	}
}

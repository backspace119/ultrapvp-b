package com.me.backspace119.utils.handlers.player;

import java.util.UUID;

import org.bukkit.entity.Player;

import com.me.backspace119.enums.Badge;
import com.me.backspace119.utils.handlers.ConfigHandler;

public class BadgeHandler {

	static ConfigHandler config;

	public BadgeHandler(ConfigHandler config) {
		BadgeHandler.config = config;
	}

	public static boolean hasBadge(Player player, Badge badge) {
		return config.getConfig().getBoolean(player.getName() + badge.getName());
	}

	public static void giveBadge(UUID name, Badge badge) {
		config.getConfig().set(name + badge.getName(), true);
		PlayerXPHandler.addXP(name, badge.getXPReward());
		PlayerXPHandler.addCoins(name, badge.getKillCoinReward());
		PlayerXPHandler.addPrestige(name, badge.getPrestige());
	}
}

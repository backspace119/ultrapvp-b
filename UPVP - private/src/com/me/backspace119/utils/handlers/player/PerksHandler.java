package com.me.backspace119.utils.handlers.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.me.backspace119.UltraPVP;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.utils.Utils;

public class PerksHandler {

	static Map<Player, List<Perk>> perksMap = new HashMap<Player, List<Perk>>();
	static JavaPlugin plugin;

	public PerksHandler(JavaPlugin plugin) {
		PerksHandler.plugin = plugin;
	}

	public static void newPlayer(Player player) {
		List<Perk> perks = new ArrayList<Perk>();
		for (Perk perk : PlayerXPHandler.getPerks(player.getUniqueId())) {
			perks.add(perk);
		}
		perksMap.put(player, perks);
	}

	public static boolean hasPerk(Player player, Perk perk) {
		return perksMap.get(player).contains(perk);
	}

	public static void reset() {
		perksMap.clear();
	}

	public static void removePlayer(Player player) {
		perksMap.remove(player);
	}

	private final static void setPerk(Player player, Perk perk) {
		if (UltraPVP.perksEnabled) {
			if (Utils.getPlayerMap().containsKey(player)) {
				List<Perk> perks = perksMap.get(player);
				perks.add(perk);
				perksMap.put(player, perks);
				player.sendMessage(ChatColor.DARK_GREEN + perk.getFaceName() + " has recharged");
			}
		}
	}

	public static void resetPerk(final Player player, final Perk perk) {
		if (UltraPVP.perksEnabled) {
			List<Perk> perks = perksMap.get(player);
			perks.remove(perk);
			perksMap.put(player, perks);
			Utils.getGameMap().get(Utils.getPlayerMap().get(player)).broadcast(ChatColor.GOLD + player.getName() + " has used " + perk.getFaceName() + "!");
			Utils.getGameByPlayer(player).getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

				@Override
				public void run() {

					setPerk(player, perk);

				}

			}, (long) (perk.getCooldown() - (perk.getModifier() * PlayerXPHandler.getPerkLevel(player.getUniqueId(), perk))) * 20));
		}
	}
}

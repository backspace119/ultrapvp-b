package com.me.backspace119.utils.handlers.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import com.me.backspace119.UltraPVP;
import com.me.backspace119.classes.PlayerClass;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.utils.handlers.ConfigHandler;

public class PlayerXPHandler {

	public static ConfigHandler config;
	public static ConfigHandler classesConfig;
	static JavaPlugin plugin;
	static int difficulty;
	static Map<String, PlayerClass> classes = new HashMap<String, PlayerClass>();

	public PlayerXPHandler(ConfigHandler config, JavaPlugin plugin, int difficulty, ConfigHandler classesConfig) {
		PlayerXPHandler.config = config;
		PlayerXPHandler.plugin = plugin;
		PlayerXPHandler.difficulty = difficulty;
		PlayerXPHandler.classesConfig = classesConfig;
	}

	public static Map<String, PlayerClass> getClasses() {
		return classes;
	}

	public static void addClass(PlayerClass playerClass, String name) {
		classes.put(name, playerClass);
	}

	public static ConfigHandler getClassConfigHandler() {
		return classesConfig;
	}

	public static ConfigHandler config() {
		return config;
	}

	public static void addHead(UUID name, String head) {
		List<String> heads = new ArrayList<String>();
		heads = config.getConfig().getStringList(name.toString() + ".heads");

		heads.add(head);
		config.getConfig().set(name.toString() + ".heads", heads);
	}

	public static void setPrestige(UUID name, int prestige) {
		config.getConfig().set(name.toString() + ".prestige", prestige);
	}

	public static void addPrestige(UUID name, int amount) {
		config.getConfig().set(name.toString() + ".prestige", getPrestige(name) + amount);
	}

	public static int getPrestige(UUID name) {
		return config.getConfig().getInt(name.toString() + ".prestige");
	}

	public static List<String> getHeads(UUID name) {
		List<String> heads = new ArrayList<String>();
		heads = config.getConfig().getStringList(name.toString() + ".heads");
		return heads;

	}

	public static void addCoins(UUID name, int amount) {
		setCoins(name, getCoins(name) + amount);
	}

	public static void clearHeads(UUID name) {
		List<String> cleared = new ArrayList<String>();
		config.getConfig().set(name.toString() + ".heads", cleared);
	}

	public static int getXP(UUID name) {
		return config.getConfig().getInt(name.toString() + ".xp");
	}

	public static boolean hasPlayer(UUID name) {
		return config.getConfig().get(name.toString()) != null;
	}

	public static int getLevel(UUID name) {
		return config.getConfig().getInt(name.toString() + ".level");
	}

	public static void setCoins(UUID name, int amount) {
		config.getConfig().set(name.toString() + ".coins", amount);
	}

	public static void addXP(UUID name, int amount) {
		config.getConfig().set(name.toString() + ".xp", config.getConfig().getInt(name.toString() + ".xp") + amount);
		while (levelup(config.getConfig().getInt(name.toString() + ".xp"), config.getConfig().getInt(name.toString() + ".level"))) {
			config.getConfig().set(name.toString() + ".level", config.getConfig().getInt(name.toString() + ".level") + 1);
			if (UltraPVP.perksEnabled) {
				plugin.getServer().getPlayer(name).sendMessage(ChatColor.GREEN + "You have leveled up to level " + ChatColor.GOLD + config.getConfig().getInt((name.toString() + ".level")) + ChatColor.GREEN + ". You have been awarded 2 ability points to spend on abilities and ability levels. Type /upvp abilities for a list of abilities. Type /upvp getAbility to add a ability.");
			} else {
				plugin.getServer().getPlayer(name).sendMessage(ChatColor.GREEN + "You have leveled up to level " + ChatColor.GOLD + config.getConfig().getInt((name.toString() + ".level")));
			}
			addPerkPoint(name);
			addPerkPoint(name);
		}

		//if (rankUp(name)) {
		//	plugin.getServer().getPlayer(name).sendMessage(ChatColor.BLUE + "You have risen in rank! Type /upvp ranked to see your spot!");
		//}
	}

	public static void save() {
		config.saveConfig();
	}

	public static int getCoins(UUID name) {
		return config.getConfig().getInt(name.toString() + ".coins");
	}

	private static boolean levelup(int xp, int level) {
		if (level > 25) {
			if (Math.log(xp) / Math.log(1.9 + (difficulty / 1000000)) > (level + 1)) {
				return true;
			}
		} else if (level > 18) {
			if (Math.log(xp) / Math.log(1.93 + (difficulty / 1000000)) > (level + 1)) {
				return true;
			}
		} else if (level > 14) {
			if (Math.log(xp) / Math.log(1.95 + (difficulty / 1000000)) > (level + 1)) {
				return true;
			}
		} else if (level > 5) {
			if (Math.log(xp) / Math.log(2 + (difficulty / 1000000)) > (level + 1)) {
				return true;
			}
		} else {
			if ((Math.log(xp - (50 * level)) / Math.log(2)) > (level + 1)) {
				return true;
			}
		}

		return false;
	}

	public static void addTie(UUID name) {
		config.getConfig().set(name.toString() + ".ties", config.getConfig().getInt(name.toString() + ".ties") + 1);
		config.getConfig().set(name.toString() + ".games", config.getConfig().getInt(name.toString() + ".games") + 1);
		save();
	}

	public static void addLoss(UUID name) {
		config.getConfig().set(name.toString() + ".losses", config.getConfig().getInt(name + ".losses") + 1);
		config.getConfig().set(name.toString() + ".games", config.getConfig().getInt(name + ".games") + 1);
		save();
	}

	public static void addWin(UUID name) {
		config.getConfig().set(name.toString() + ".wins", config.getConfig().getInt(name + ".wins") + 1);
		config.getConfig().set(name.toString() + ".games", config.getConfig().getInt(name + ".games") + 1);
		save();
	}

	public static int getWins(UUID name) {
		return config.getConfig().getInt(name.toString() + ".wins");
	}

	public static int getLosses(UUID name) {
		return config.getConfig().getInt(name.toString() + ".losses");
	}

	public static int getTies(UUID name) {
		return config.getConfig().getInt(name.toString() + ".ties");
	}

	public static int getPerkPoints(UUID name) {
		return config.getConfig().getInt(name.toString() + ".perkPoints");
	}

	public static PlayerClass getClass(UUID name) {
		return classes.get(config.getConfig().getString(name.toString() + ".class"));
	}

	public static void setClass(UUID name, PlayerClass c) {
		config.getConfig().set(name.toString() + ".class", c.getName());
	}

	public static boolean addPerk(Perk perk, UUID name) {
		List<String> perks = config.getConfig().getStringList(name + ".perks");
		if (perks.contains(perk.name())) {
			if (config.getConfig().getInt(name.toString() + "." + perk.name()) < perk.getMaxLevel()) {
				config.getConfig().set(name.toString() + "." + perk.name(), config.getConfig().getInt(name.toString() + "." + perk.name()) + 1);
				config.getConfig().set(name.toString() + ".perkPoints", config.getConfig().getInt(name.toString() + ".perkPoints") - 1);
				save();
				return true;
			} else {
				plugin.getServer().getPlayer(name).sendMessage(ChatColor.GOLD + "That perk is at max level.");
			}
		} else {
			if (perk.getLevelRequired() > getLevel(name)) {
				plugin.getServer().getPlayer(name).sendMessage(ChatColor.GOLD + "You are not high enough level to get that yet.");
			} else {
				perks.add(perk.name());
				config.getConfig().set(name.toString() + ".perks", perks);
				config.getConfig().set(name.toString() + "." + perk.name(), 1);
				config.getConfig().set(name.toString() + ".perkPoints", config.getConfig().getInt(name.toString() + ".perkPoints") - 1);
				save();
				return true;
			}
		}

		return false;
	}

	public static void addPerkPoint(UUID name) {
		config.getConfig().set(name.toString() + ".perkPoints", config.getConfig().getInt(name.toString() + ".perkPoints") + 1);
		save();
	}

	public static List<Perk> getPerks(UUID name) {
		List<String> list = config.getConfig().getStringList(name.toString() + ".perks");
		List<Perk> perks = new ArrayList<Perk>();
		for (String perk : list) {
			perks.add(Perk.valueOf(perk));
		}
		return perks;
	}

	public static int getPerkLevel(UUID name, Perk perk) {
		if (getPerks(name).contains(perk)) {
			return config.getConfig().getInt(name.toString() + "." + perk.name());
		} else {
			return 0;
		}
	}

	private static boolean rankUp(UUID id) {

		List<String> rankedRaw = config.getConfig().getStringList("ranked");
		List<UUID> ranked = new ArrayList<UUID>();
		for(String raw: rankedRaw)
		{
			ranked.add(UUID.fromString(raw));
		}
		for (int i = 0; i < 5; i++) {
			if (!ranked.get(i).equals(id)) {
				if (config.getConfig().getInt(ranked.get(i) + ".xp") < config.getConfig().getInt(id.toString() + ".xp")) {

					config.getConfig().set(id.toString() + ".rank", i + 1);
					UUID pass = ranked.get(i);
					//ranked.remove(i);
					ranked.add(i, id);

					config.getConfig().set("ranked", ranked);
					if (!rankUp(pass)) {
						config.getConfig().set(pass + ".rank", -1);
					}
					return true;
				}
			} else {
				return false;
			}
		}
		return false;
	}

	public static List<UUID> getRanked() {
		List<String> rankedRaw = config.getConfig().getStringList("ranked");
		List<UUID> ranked = new ArrayList<UUID>();
		for(String raw: rankedRaw)
		{
			ranked.add(UUID.fromString(raw));
		}
		return ranked;
	}

	public static int getRank(UUID name) {
		return config.getConfig().getInt(name.toString() + ".rank");
	}
}

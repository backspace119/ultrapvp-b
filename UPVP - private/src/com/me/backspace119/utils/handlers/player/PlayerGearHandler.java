package com.me.backspace119.utils.handlers.player;

import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.me.backspace119.utils.handlers.ConfigHandler;

public class PlayerGearHandler {

	private static ConfigHandler configHandler;
	private static JavaPlugin plugin;

	public PlayerGearHandler(ConfigHandler configHandler, JavaPlugin plugin) {
		PlayerGearHandler.configHandler = configHandler;
		PlayerGearHandler.plugin = plugin;
	}

	public static ItemStack[] defaultGear() {
		ItemStack[] inventory = new ItemStack[36];
		inventory[0] = new ItemStack(Material.STONE_SWORD, 1);
		inventory[0].addEnchantment(Enchantment.DURABILITY, 3);
		inventory[1] = new ItemStack(Material.BOW, 1);
		inventory[1].addEnchantment(Enchantment.ARROW_INFINITE, 1);
		inventory[1].addEnchantment(Enchantment.DURABILITY, 3);
		inventory[2] = new ItemStack(Material.ARROW, 1);
		inventory[3] = new ItemStack(Material.IRON_SPADE, 1);
		inventory[3].addEnchantment(Enchantment.DURABILITY, 3);
		inventory[4] = new ItemStack(Material.REDSTONE_TORCH_ON, 8);
		return inventory;
	}

	public static ItemStack[] getClassInventory(String className) {
		ItemStack[] inventory = new ItemStack[35];
		int i = 0;
		for (String key : configHandler.getConfig().getConfigurationSection(className + ".inventory").getKeys(false)) {
			inventory[i] = configHandler.getConfig().getItemStack(className + ".inventory." + key);
			i++;
		}

		return inventory;
	}

	public static ItemStack[] getClassArmor(String className) {
		ItemStack[] armor = new ItemStack[4];
		int i = 0;
		for (String key : configHandler.getConfig().getConfigurationSection(className + ".armor").getKeys(false)) {
			armor[i] = configHandler.getConfig().getItemStack(className + ".armor." + key);
			i++;
		}
		return armor;
	}

	public static void setClassGear(String className, UUID name) {
		Player p = plugin.getServer().getPlayer(name);

		for (int i = 0; p.getInventory().getContents().length > i; i++) {
			configHandler.getConfig().set(className + ".inventory." + i, p.getInventory().getContents()[i]);
		}

		for (int i = 0; p.getInventory().getArmorContents().length > i; i++) {
			configHandler.getConfig().set(className + ".armor." + i, p.getInventory().getArmorContents()[i]);
		}
		configHandler.saveConfig();
	}
}

package com.me.backspace119.utils.handlers.factions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.entity.Faction;
import com.me.backspace119.UltraPVP;
import com.me.backspace119.enums.FactionPerk;
import com.me.backspace119.games.FactionGame;
import com.me.backspace119.utils.Utils;

public class FactionPerksHandler {

	static Map<Faction, List<FactionPerk>> perksMap = new HashMap<Faction, List<FactionPerk>>();
	static JavaPlugin plugin;

	FactionPerksHandler(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	public static void newFaction(Faction faction) {
		List<FactionPerk> perks = new ArrayList<FactionPerk>();
		for (FactionPerk perk : FactionXPHandler.getPerks(faction.getName())) {
			perks.add(perk);
		}
		perksMap.put(faction, perks);
	}

	public static boolean hasFactionPerk(Faction faction, FactionPerk perk) {
		return perksMap.get(faction).contains(perk);
	}

	public static void reset() {
		perksMap.clear();
	}

	public static void removeFaction(Faction faction) {
		perksMap.remove(faction);
	}

	private final static void setFactionPerk(Faction faction, FactionPerk perk, Player player) {
		if (UltraPVP.perksEnabled) {
			if (Utils.getPlayerMap().containsKey(player)) {
				List<FactionPerk> perks = perksMap.get(faction);
				perks.add(perk);
				perksMap.put(faction, perks);

				FactionGame game = (FactionGame) Utils.getGameByPlayer(player);
				for (Player p : game.getPlayersByFaction(faction.getName())) {
					p.sendMessage(ChatColor.DARK_GREEN + perk.getFaceName() + " has recharged");
				}
			}
		}
	}

	public static void resetFactionPerk(final Faction faction, final FactionPerk perk, final Player player) {
		if (UltraPVP.perksEnabled) {
			List<FactionPerk> perks = perksMap.get(faction);
			perks.remove(perk);
			perksMap.put(faction, perks);
			Utils.getGameMap().get(Utils.getPlayerMap().get(player)).broadcast(ChatColor.GOLD + faction.getName() + " has used " + perk.getFaceName() + "!");
			Utils.getGameByPlayer(player).getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

				@Override
				public void run() {

					setFactionPerk(faction, perk, player);

				}

			}, (long) (perk.getCoolDown() - (perk.getMultiplier() * FactionXPHandler.getPerkLevel(faction.getName(), perk))) * 20));
		}
	}
}

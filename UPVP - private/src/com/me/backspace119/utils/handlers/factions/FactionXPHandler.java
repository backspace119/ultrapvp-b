package com.me.backspace119.utils.handlers.factions;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.entity.Faction;
import com.me.backspace119.enums.FactionPerk;
import com.me.backspace119.utils.handlers.ConfigHandler;

public class FactionXPHandler {
	static ConfigHandler config;
	static JavaPlugin plugin;

	public FactionXPHandler(ConfigHandler config, JavaPlugin plugin) {
		FactionXPHandler.config = config;
		FactionXPHandler.plugin = plugin;
	}

	public static void newFaction(String name) {
		config.getConfig().set(name + ".xp", 0);
		config.getConfig().set(name + ".level", 1);
		List<String> factions = config.getConfig().getStringList("factions");
		factions.add(name);
		config.getConfig().set("factions", factions);
		config.getConfig().set(name + ".rank", -1);
		save();
	}

	public static int getXP(String name) {
		return config.getConfig().getInt(name + ".xp");
	}

	public static int getLevel(String name) {
		return config.getConfig().getInt(name + ".level");
	}

	public static void changeName(String oldName, String newName) {
		config.getConfig().set(newName + ".xp", config.getConfig().getInt(oldName + ".xp"));
		config.getConfig().set(newName + ".level", config.getConfig().getInt(oldName + ".level"));
		// add cleanup code here to delete the old segments. not sure how to
		// achieve that right now
		config.getConfig().set(oldName + ".xp", 0);
		config.getConfig().set(oldName + ".level", 1);
		save();
	}

	public static int getRank(String name) {
		return config.getConfig().getInt(name + ".rank");
	}

	public static void addXP(Faction faction, int amount) {

		if (!config.getConfig().getStringList("factions").contains(faction.getName())) {
			List<String> factions = config.getConfig().getStringList("factions");
			factions.add(faction.getName());
			config.getConfig().set("factions", factions);
		}
		config.getConfig().set(faction.getName() + ".xp", config.getConfig().getInt(faction.getName() + ".xp") + amount);
		while (levelUp(config.getConfig().getInt(faction.getName() + ".level"), config.getConfig().getInt(faction.getName() + ".xp"))) {
			config.getConfig().set(faction.getName() + ".level", config.getConfig().getInt(faction.getName() + ".level") + 1);
			for (Player player : faction.getOnlinePlayers()) {
				player.sendMessage(ChatColor.GREEN + "Your faction has leveled up in UPVP. Your level is now: " + ChatColor.WHITE + config.getConfig().getInt(faction.getName() + ".level"));
			}
			addPerkPoint(faction.getName());
		}
		if (rankUp(faction.getName())) {
			for (Player player : faction.getOnlinePlayers()) {
				player.sendMessage(ChatColor.GREEN + "YOUR FACTION HAS CHANGED RANK! YOU ARE NOW RANKED: " + ChatColor.WHITE + config.getConfig().getInt(faction.getName() + ".rank"));
			}
		}
		save();
	}

	private static boolean rankUp(String faction) {

		List<String> ranked = config.getConfig().getStringList("ranked");
		for (int i = 0; i < 5; i++) {
			if (!ranked.get(i).equals(faction)) {
				if (config.getConfig().getInt(ranked.get(i) + ".xp") < config.getConfig().getInt(faction + ".xp")) {

					config.getConfig().set(faction + ".rank", i + 1);
					String pass = ranked.get(i);
					// ranked.remove(i);
					ranked.add(i, faction);

					config.getConfig().set("ranked", ranked);
					if (!rankUp(pass)) {
						config.getConfig().set(pass + ".rank", -1);
					}
					return true;

				}
			} else {
				return false;
			}
		}
		return false;
	}

	public static List<String> getRanked() {
		return config.getConfig().getStringList("ranked");
	}

	public static void save() {
		config.saveConfig();
	}

	private static boolean levelUp(int level, int xp) {
		if ((Math.log(xp - (150 * level)) / Math.log(2)) > (level + 1)) { return true; }

		return false;
	}

	public static boolean addPerk(FactionPerk perk, String name, String player) {
		List<String> perks = config.getConfig().getStringList(name + ".perks");
		if (perks.contains(perk.name())) {
			if (config.getConfig().getInt(name + "." + perk.name()) < perk.getMaxLevel()) {
				config.getConfig().set(name + "." + perk.name(), config.getConfig().getInt(name + "." + perk.name()) + 1);
				config.getConfig().set(name + ".perkPoints", config.getConfig().getInt(name + ".perkPoints") - 1);
				save();
				return true;
			} else {
				plugin.getServer().getPlayer(player).sendMessage(ChatColor.GOLD + "That perk is at max level.");
			}
		} else {
			if (perk.getLevelRequired() > getLevel(name)) {
				// plugin.getServer().getPlayer(name).sendMessage(ChatColor.GOLD
				// + "You are not high enough level to get that yet.");
			} else {
				perks.add(perk.name());
				config.getConfig().set(name + ".perks", perks);
				config.getConfig().set(name + "." + perk.name(), 1);
				config.getConfig().set(name + ".perkPoints", config.getConfig().getInt(name + ".perkPoints") - 1);
				save();
				return true;
			}
		}

		return false;
	}

	public static void addPerkPoint(String name) {
		config.getConfig().set(name + ".perkPoints", config.getConfig().getInt(name + ".perkPoints") + 1);
		save();
	}

	public static List<FactionPerk> getPerks(String name) {
		List<String> list = config.getConfig().getStringList(name + ".perks");
		List<FactionPerk> perks = new ArrayList<FactionPerk>();
		for (String perk : list) {
			perks.add(FactionPerk.valueOf(perk));
		}
		return perks;
	}

	public static int getPerkLevel(String name, FactionPerk perk) {
		if (getPerks(name).contains(perk)) {
			return config.getConfig().getInt(name + "." + perk.name());
		} else {
			return 0;
		}
	}
}

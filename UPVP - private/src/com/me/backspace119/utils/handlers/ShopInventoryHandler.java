package com.me.backspace119.utils.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class ShopInventoryHandler {

	static JavaPlugin plugin;
	private static ItemStack[] shopInventory_1;
	static Map<Integer, Integer> inventory_1_CostMap = new HashMap<Integer, Integer>();
	static Logger logger;
	static ConfigHandler config;

	public ShopInventoryHandler(JavaPlugin plugin, Logger logger, ConfigHandler config) {
		ShopInventoryHandler.config = config;
		ShopInventoryHandler.plugin = plugin;
		ShopInventoryHandler.logger = logger;
		setupInventories();
	}

	/**
	 * rewritten somewhat at version 1.5.3 this was one of the first things i
	 * wrote and it definitely shows :/ it really needs more of a revamp when i
	 * have more time for it.
	 */
	private static void setupInventories() {
		// TODO rewrite everything here!
		ItemStack[] inventory_1 = new ItemStack[27];
		int i = 0;
		String[] pass;
		for (String key : config.getConfig().getKeys(false)) {
			try {

				inventory_1[i] = new ItemStack(Material.valueOf(key), config.getConfig().getInt(key + ".amount"));
				ItemMeta meta = inventory_1[i].getItemMeta();
				List<String> list = new ArrayList<String>();
				list.add(config.getConfig().getString(key + ".kills") + " kills");

				meta.setLore(list);
				inventory_1[i].setItemMeta(meta);
				if (config.getConfig().getString(key + ".name") != null) {
					inventory_1[i].getItemMeta().setDisplayName(config.getConfig().getString(key + ".name"));
				}
				if (Material.valueOf(key).equals(Material.EGG)) {
					inventory_1[i].getItemMeta().setDisplayName("Grenade");
				} else if (Material.valueOf(key).equals(Material.BLAZE_ROD)) {
					inventory_1[i].getItemMeta().setDisplayName("Rocket Launcher");

				}

				if (config.getConfig().getString(key + ".info") != null) {
					List<String> lore = new ArrayList<String>();
					lore.add(config.getConfig().getString(key + ".info"));
					inventory_1[i].getItemMeta().setLore(lore);
				}
				inventory_1_CostMap.put(i, config.getConfig().getInt(key + ".kills"));
				List<String> l = config.getConfig().getStringList(key + ".enchantments");
				for (String ench : l) {
					Enchantment enchantment = enchantDecoder(ench);
					pass = ench.split(" ");
					if (enchantment != null && pass.length > 1 && (Object) Integer.parseInt(pass[1]) != null) {
						logger.info("[UPVP] applying enchantment " + enchantment.getName() + " to item " + inventory_1[i].getType().name());

						inventory_1[i].addEnchantment(enchantment, Integer.parseInt(pass[1]));
					} else {
						logger.severe("[UPVP] Bad inventory entry in config please check the node: " + key + ".enchantments");

					}

				}
			} catch (Exception e) {
				System.out.println("Issue setting up shop inventories: ");
				e.printStackTrace();
			}
			i++;
		}

		shopInventory_1 = inventory_1;

	}

	public static ItemStack[] getDefaultInventory() {
		return shopInventory_1;
	}

	public static Map<Integer, Integer> getDefaultInventoryCostMap() {
		return inventory_1_CostMap;
	}

	private static Enchantment enchantDecoder(String input) {
		if (input.contains("unbreaking")) {

			return Enchantment.DURABILITY;
		} else if (input.contains("sharpness")) {
			return Enchantment.DAMAGE_ALL;
		} else if (input.contains("knockback")) {
			return Enchantment.KNOCKBACK;
		} else if (input.contains("infinity")) {
			return Enchantment.ARROW_INFINITE;
		} else if (input.contains("power")) {
			return Enchantment.ARROW_DAMAGE;
		} else if (input.contains("punch")) {
			return Enchantment.ARROW_KNOCKBACK;
		} else if (input.contains("flame")) {
			return Enchantment.ARROW_FIRE;
		} else if (input.contains("fire_aspect")) {
			return Enchantment.FIRE_ASPECT;
		} else if (input.contains("protection")) {
			return Enchantment.PROTECTION_ENVIRONMENTAL;
		} else if (input.contains("blast protection")) {
			return Enchantment.PROTECTION_EXPLOSIONS;
		} else if (input.contains("fire protection")) {
			return Enchantment.PROTECTION_FIRE;
		} else if (input.contains("projectile protection")) { return Enchantment.PROTECTION_PROJECTILE; }
		return null;
	}

}

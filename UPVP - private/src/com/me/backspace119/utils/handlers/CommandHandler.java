package com.me.backspace119.utils.handlers;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.UPlayer;
import com.me.backspace119.UltraPVP;
import com.me.backspace119.classes.ClassAttributes;
import com.me.backspace119.classes.PlayerClass;
import com.me.backspace119.enums.AttributeType;
import com.me.backspace119.enums.GameTypes;
import com.me.backspace119.enums.Perk;
import com.me.backspace119.factories.GameFactory;
import com.me.backspace119.games.RedBlueTeamGame;
import com.me.backspace119.utils.Utils;
import com.me.backspace119.utils.handlers.factions.FactionXPHandler;
import com.me.backspace119.utils.handlers.player.PlayerGearHandler;
import com.me.backspace119.utils.handlers.player.PlayerXPHandler;

public class CommandHandler implements CommandExecutor {

	JavaPlugin plugin;
	ConfigHandler configHandler;

	public CommandHandler(JavaPlugin plugin, ConfigHandler configHandler) {
		this.plugin = plugin;
		this.configHandler = configHandler;
	}

	@Override
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLbl, String[] args) {
		if (cmdLbl.equalsIgnoreCase("upvp")) {
			if (!(sender instanceof Player)) {
				if (args[0].equalsIgnoreCase("giveXP")) {
					if (args.length < 3) {

					} else {
						PlayerXPHandler.addXP(plugin.getServer().getPlayer(args[1]).getUniqueId(), Integer.parseInt(args[2]));
					}
				}
			} else {
				Player player = (Player) sender;

				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("noChat")) {
						if (Utils.getPlayerMap().containsKey(player)) {
							Utils.getGameMap().get(Utils.getPlayerMap().get(player)).noChat(player);
						} else {
							player.sendMessage(ChatColor.RED + "You are not part of a game");
						}
					} else if (args[0].equalsIgnoreCase("xp")) {
						if (args.length > 1) {
							if (PlayerXPHandler.hasPlayer(plugin.getServer().getPlayer(args[1]).getUniqueId())) {
								player.sendMessage(ChatColor.GOLD + "Player: " + ChatColor.GREEN + args[1] + ChatColor.GOLD + " has " + ChatColor.WHITE + PlayerXPHandler.getXP(plugin.getServer().getPlayer(args[1]).getUniqueId()) + ChatColor.GOLD + " xp and is level: " + ChatColor.WHITE + PlayerXPHandler.getLevel(plugin.getServer().getPlayer(args[1]).getUniqueId()));
							} else {
								player.sendMessage(ChatColor.RED + "Player: " + ChatColor.WHITE + args[1] + ChatColor.RED + " does not exist!");
							}
						} else {
							player.sendMessage(ChatColor.GOLD + "You have: " + ChatColor.WHITE + PlayerXPHandler.getXP(player.getUniqueId()) + ChatColor.GOLD + " xp and are level: " + ChatColor.WHITE + PlayerXPHandler.getLevel(player.getUniqueId()));
						}
					} else if (args[0].equalsIgnoreCase("buyCoins")) {
						if (UltraPVP.hasEconomy) {
							if (args.length > 1) {
								if ((Object) Double.parseDouble(args[1]) != null) {
									if (UltraPVP.econ.getBalance(player.getName()) < configHandler.getConfig().getDouble("ExchangeRateBuy") * Double.parseDouble(args[1])) {
										player.sendMessage(ChatColor.RED + "Not enough money to buy that amount of coins");
									} else {
										PlayerXPHandler.setCoins(player.getUniqueId(), Utils.getGlobalCoins().get(player) + Integer.parseInt(args[1]));
										Utils.getGlobalCoins().put(player, Utils.getGlobalCoins().get(player) + Integer.parseInt(args[1]));
										UltraPVP.econ.withdrawPlayer(player.getName(), configHandler.getConfig().getDouble("ExchangeRateBuy") * Double.parseDouble(args[1]));
										player.sendMessage("successfully bought " + args[1] + " coins");
										PlayerXPHandler.save();
									}
								} else {
									player.sendMessage(ChatColor.RED + "Invalid number of coins");
								}
							} else {
								player.sendMessage(ChatColor.RED + "Please input the number of coins you would like to buy.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "No economy plugin on this server. Coins cannot be bought.");
						}
					} else if (args[0].equalsIgnoreCase("sellCoins")) {
						if (UltraPVP.hasEconomy) {
							if (args.length > 1) {
								if ((Object) Integer.parseInt(args[1]) != null) {
									if (Utils.getGlobalCoins().get(player) < Integer.parseInt(args[1])) {
										player.sendMessage(ChatColor.RED + "Not enough coins to sell");
									} else {
										PlayerXPHandler.setCoins(player.getUniqueId(), Utils.getGlobalCoins().get(player) - Integer.parseInt(args[1]));
										Utils.getGlobalCoins().put(player, Utils.getGlobalCoins().get(player) - Integer.parseInt(args[1]));
										UltraPVP.econ.depositPlayer(player.getName(), Double.parseDouble(args[1]) * configHandler.getConfig().getDouble("ExchangeRateSell"));
										player.sendMessage(ChatColor.GREEN + "Successfully sold " + ChatColor.GOLD + args[1] + ChatColor.GREEN + " coins");
										PlayerXPHandler.save();
									}
								} else {
									player.sendMessage(ChatColor.RED + "Invalid number of coins");
								}
							} else {
								player.sendMessage(ChatColor.RED + "Please input the number of coins you would like to sell.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "No economy plugin on this server. Coins cannot be sold.");
						}
					} else if (args[0].equalsIgnoreCase("coins")) {
						if (args.length > 1) {
							if (PlayerXPHandler.hasPlayer(plugin.getServer().getPlayer(args[1]).getUniqueId())) {
								player.sendMessage("Player " + ChatColor.GOLD + args[1] + ChatColor.WHITE + " has " + ChatColor.GOLD + PlayerXPHandler.getCoins(plugin.getServer().getPlayer(args[1]).getUniqueId()) + ChatColor.WHITE + " coins");
							} else {
								player.sendMessage(ChatColor.RED + "Player " + ChatColor.GOLD + args[1] + ChatColor.RED + " does not exist!");
							}
						} else {
							player.sendMessage("You have " + ChatColor.GOLD + PlayerXPHandler.getCoins(player.getUniqueId()) + ChatColor.WHITE + " coins");
						}
					} else if (args[0].equalsIgnoreCase("tradeIn")) {
						if (args.length > 1) {
							if ((Object) Integer.parseInt(args[1]) != null) {
								if (Utils.getGlobalCoins().get(player) < Integer.parseInt(args[1])) {
									player.sendMessage(ChatColor.RED + "Not enough coins to trade in");
								} else {
									PlayerXPHandler.setCoins(player.getUniqueId(), Utils.getGlobalCoins().get(player) - Integer.parseInt(args[1]));
									Utils.getGlobalCoins().put(player, Utils.getGlobalCoins().get(player) - Integer.parseInt(args[1]));

									PlayerXPHandler.addXP(player.getUniqueId(), configHandler.getConfig().getInt("xpTradeInRate") * Integer.parseInt(args[1]));
									player.sendMessage(ChatColor.GREEN + "Successfully traded in " + ChatColor.GOLD + args[1] + ChatColor.GREEN + " coins");
									PlayerXPHandler.save();
								}
							} else {
								player.sendMessage(ChatColor.RED + "Invalid number of coins");
							}
						} else {
							player.sendMessage(ChatColor.RED + "Please input the amount of coins to trade for XP");
						}
					} else if (args[0].equalsIgnoreCase("leave")) {
						if (Utils.getPlayerMap().containsKey(player)) {
							if (Utils.getGameByPlayer(player).getSpectators().contains(player)) {
								Utils.getGameByPlayer(player).removeSpectator(player);
							} else {
								Utils.kickPlayer(player.getUniqueId());
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in an UltraPVP game");
						}
					} else if (args[0].equalsIgnoreCase("givemeperk")) {
						// shouldn't it check isOp first?
						if (UltraPVP.perksEnabled) {
							if (args.length > 1) {
								if (player.isOp()) {
									PlayerXPHandler.addPerk(Perk.valueOf(args[1]), player.getUniqueId());
								} else {
									player.sendMessage(ChatColor.RED + "You do not have permission");
								}
							} else {
								player.sendMessage(ChatColor.RED + "A perk name is required");
							}
						} else {
							player.sendMessage(ChatColor.RED + "Abilities are disabled on this server. To enable them, go to the plugins folder and into the UltraPVP folder and find a file named upvp.yml. Open it and change perksEnabled: to true");
						}
					} else if (args[0].equalsIgnoreCase("getAbility")) {
						if (UltraPVP.perksEnabled) {
							if (PlayerXPHandler.getPerkPoints(player.getUniqueId()) > 0) {
								if (args.length > 1) {
									for (Perk perk : Perk.values()) {
										if (perk.getFaceName().equalsIgnoreCase(args[1])) {
											if (PlayerXPHandler.addPerk(perk, player.getUniqueId())) {
												player.sendMessage(ChatColor.GREEN + "Successfully added ability: " + ChatColor.GOLD + perk.getFaceName() + ChatColor.GREEN + " to your list of abilities. Its level is: " + ChatColor.GOLD + PlayerXPHandler.getPerkLevel(player.getUniqueId(), perk) + ". " + ChatColor.GREEN + "To level it up, just add it as an ability again.");
												return true;
											}
											return false;
										}
									}

									player.sendMessage(ChatColor.RED + "No ability with that name found");
								} else {
									player.sendMessage(ChatColor.RED + "Not enough arguments. Syntax: /upvp getAbility <AbilityName>");
								}
							} else {
								player.sendMessage(ChatColor.RED + "No points to spend");
							}
						} else {
							player.sendMessage(ChatColor.RED + "Abilities are disabled on this server");
						}
					} else if (args[0].equalsIgnoreCase("abilities")) {
						if (UltraPVP.perksEnabled) {
							for (Perk perk : Perk.values()) {
								player.sendMessage(ChatColor.GREEN + perk.getFaceName() + ": " + ChatColor.WHITE + perk.description());
							}
						} else {
							player.sendMessage(ChatColor.RED + "Abilities are disabled on this server");
						}
					} else if (args[0].equalsIgnoreCase("factionXP")) {
						UPlayer uplayer = UPlayer.get(player);
						Faction faction = uplayer.getFaction();
						player.sendMessage("Your faction is level: " + ChatColor.GOLD + FactionXPHandler.getLevel(faction.getName()) + ChatColor.WHITE + " and has " + ChatColor.GOLD + FactionXPHandler.getXP(faction.getName()) + ChatColor.WHITE + " XP");
					} else if (args[0].equalsIgnoreCase("stats")) {
						if (args.length > 1) {
							if (PlayerXPHandler.hasPlayer(plugin.getServer().getPlayer(args[1]).getUniqueId())) {
								player.sendMessage("Player " + ChatColor.GOLD + args[1] + ChatColor.WHITE + " has " + ChatColor.GOLD + PlayerXPHandler.getWins(plugin.getServer().getPlayer(args[1]).getUniqueId()) + ChatColor.WHITE + " wins, " + ChatColor.GOLD + PlayerXPHandler.getLosses(plugin.getServer().getPlayer(args[1]).getUniqueId()) + ChatColor.WHITE + " losses, and " + ChatColor.GOLD + PlayerXPHandler.getTies(plugin.getServer().getPlayer(args[1]).getUniqueId()) + ChatColor.WHITE + " ties");
							} else {
								player.sendMessage(ChatColor.RED + "Player " + ChatColor.GOLD + args[1] + ChatColor.RED + " does not exist!");
							}
						} else {
							player.sendMessage("You have " + ChatColor.GOLD + PlayerXPHandler.getWins(player.getUniqueId()) + ChatColor.WHITE + " wins, " + ChatColor.GOLD + PlayerXPHandler.getLosses(player.getUniqueId()) + ChatColor.WHITE + " losses, and " + ChatColor.GOLD + PlayerXPHandler.getTies(player.getUniqueId()) + ChatColor.WHITE + " ties");
						}
					} else if (args[0].equalsIgnoreCase("abilityPoints")) {
						player.sendMessage("You have " + ChatColor.GOLD + PlayerXPHandler.getPerkPoints(player.getUniqueId()) + ChatColor.WHITE + " ability points.");
					} else if (args[0].equalsIgnoreCase("rank")) {
						if (UltraPVP.hasFactions) {
							UPlayer uplayer = UPlayer.get(player);
							Faction faction = uplayer.getFaction();
							if (FactionXPHandler.getRank(faction.getName()) != -1) {
								player.sendMessage(ChatColor.WHITE + "Your faction is ranked " + ChatColor.GOLD + FactionXPHandler.getRank(faction.getName()));
							} else {
								player.sendMessage("Your faction is not ranked");
							}
						}
						if (PlayerXPHandler.getRank(player.getUniqueId()) != -1) {
							player.sendMessage("You are ranked " + ChatColor.GOLD + PlayerXPHandler.getRank(player.getUniqueId()));
						} else {
							player.sendMessage("You are not ranked");
						}
					} else if (args[0].equalsIgnoreCase("ranked")) {
						if (UltraPVP.hasFactions) {
							player.sendMessage("Factions:");
							for (int i = 0; i < 5; i++) {
								player.sendMessage(ChatColor.GOLD + "" + (i + 1) + ":" + FactionXPHandler.getRanked().get(i));
							}
						}
						player.sendMessage(ChatColor.BOLD + "Players:");
						for (int i = 0; i < 5; i++) {
							player.sendMessage(ChatColor.GOLD + "" + (i + 1) + ":" + PlayerXPHandler.getRanked().get(i));
						}
						return true;
					} else if (args[0].equalsIgnoreCase("setClass")) {
						if (UltraPVP.classesEnabled) {
							if (args.length > 1) {
								for (PlayerClass c : PlayerXPHandler.getClasses().values()) {
									if (args[1].equalsIgnoreCase(c.getName())) {
										if (PlayerXPHandler.getLevel(player.getUniqueId()) < c.levelRequired()) {
											player.sendMessage(ChatColor.RED + "You are not high enough level to attain that class");
											return false;
										}

										player.sendMessage(ChatColor.GREEN + "You have successfully switched classes");
										PlayerXPHandler.setClass(player.getUniqueId(), c);
										PlayerXPHandler.save();
										return true;
									}
								}
								player.sendMessage(ChatColor.RED + "No class with that name exists");
							} else {
								player.sendMessage(ChatColor.RED + "Please enter class to change to");
							}
						} else {
							player.sendMessage(ChatColor.RED + "Classes are disabled on this server");
						}
					} else if (args[0].equalsIgnoreCase("classes")) {
						if (UltraPVP.classesEnabled) {
							for (PlayerClass c : PlayerXPHandler.getClasses().values()) {
								player.sendMessage(ChatColor.GOLD + c.getName() + ChatColor.WHITE + " " + c.description());
							}
						} else {
							player.sendMessage(ChatColor.RED + "Classes are disabled on this server");
						}
					} else if (args[0].equalsIgnoreCase("disguise")) {
						if (Utils.getPlayerMap().containsKey(player)) {
							if (!Utils.getTagged().contains(player.getUniqueId())) {
								if (PlayerXPHandler.getClass(player.getUniqueId()).getAttributes().hasChangeCostume()) {
									if (Utils.getGameByPlayer(player).getDisguised().contains(player.getUniqueId())) {
										player.sendMessage("You are already disguised");
									} else {
										Utils.getGameByPlayer(player).getDisguised().add(player.getUniqueId());
										if (Utils.getGameByPlayer(player).getType().isTeam()) {
											RedBlueTeamGame game = (RedBlueTeamGame) Utils.getGameByPlayer(player);
											if (game.getRedPlayers().contains(player)) {
												player.getInventory().setArmorContents(game.getBlueArmor());
											} else {
												player.getInventory().setArmorContents(game.getRedArmor());
											}
										}
										player.sendMessage(ChatColor.GREEN + "You have been disguised");
									}
								} else {
									player.sendMessage(ChatColor.RED + "Your class can not disguise!");
								}
							} else {
								player.sendMessage(ChatColor.RED + "You can not disguise while tagged");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a game");
						}
					} else if (args[0].equalsIgnoreCase("tank")) {
						if (Utils.getPlayerMap().containsKey(player)) {
							if (PlayerXPHandler.getClass(player.getUniqueId()).getAttributes().hasAura()) {
								if (!Utils.getTagged().contains(player.getUniqueId())) {
									Utils.getGameByPlayer(player).getTanking().add(player.getUniqueId());
									Utils.getTagged().add(player.getUniqueId());
									final Player tank = player;
									Utils.getGameByPlayer(player).getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

										@Override
										public void run() {
											Utils.getGameByPlayer(tank).getTanking().remove(tank.getUniqueId());
										}

									}, 200));

									Utils.getGameByPlayer(player).getTaskList().add(plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

										@Override
										public void run() {
											Utils.getTagged().remove(tank.getUniqueId());
											tank.sendMessage(ChatColor.GREEN + "You have regained your strength");
										}

									}, 20 * 240));
									player.sendMessage(ChatColor.GREEN + "You have produced an impenetrable aura lasting " + ChatColor.GOLD + "10" + ChatColor.GREEN + " seconds");
								} else {
									player.sendMessage(ChatColor.GOLD + "Your strength is not high enough to produce the aura. Please wait to regain your strength.");
								}
							} else {
								player.sendMessage(ChatColor.RED + "You are not a tank");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a game");
						}
					} else if (args[0].equalsIgnoreCase("createGame")) {
						if (args.length < 3) {
							player.sendMessage(ChatColor.RED + "Not enough arguments. Syntax: /upvp createGame <game name> <game type>. For valid types, use /upvp gameTypes or /upvp help types."); // this needs to be changed
						} else {
							for (GameTypes t : GameTypes.values()) {
								if (args[2].equalsIgnoreCase(t.getName())) {
									World newGame = player.getWorld();
									newGame.save();
									newGame.setAutoSave(false);
									new GameFactory(player, newGame, args[1], t, t.isTeam());
									if (t.isTeam()) {
										player.sendMessage("You will now be prompted to complete a few steps to create a new game. The world you are in has already been designated as the new game world and the current setup will be reloaded at the end of each game. If you have not finished setting up the world, type /upvp stop. " + ChatColor.GREEN + "For step 1 please stand inside the center of the red base where you want red players to spawn at. Then type /upvp next");
									} else {
										player.sendMessage("You will now be prompted to complete a few steps to create a new game. The world you are in has already been designated as the new game world and the current setup will be reloaded at the end of each game. If you have not finished setting up the world, type /upvp stop. " + ChatColor.GREEN + "For step 1 please stand inside the center of the first spot where you want players to spawn (there can be unlimited spots for them to spawn at) and type /upvp next if it's the only spawn location or /upvp another if there are more.");
									}
									Utils.startedCreation(player);
									return true;
								}
							}
							player.sendMessage(ChatColor.RED + "Unrecognized game type. To get a list of valid types, do /upvp gameTypes.");
						}
					} else if (args[0].equalsIgnoreCase("help")) {
						if (args.length < 2) {
							String[] sections = UltraPVP.helpFile.getConfig().getString("description").split("/n");
							for (String section : sections) {
								player.sendMessage(ChatColor.GREEN + section);
							}
							return true;
						}

						for (String section : UltraPVP.helpFile.getConfig().getKeys(true)) {
							if (section.equalsIgnoreCase(args[1])) {
								player.sendMessage(UltraPVP.helpFile.getConfig().getString(args[1].toLowerCase()));
								return true;
							}
						}

						player.sendMessage(ChatColor.RED + "No such UPVP command or help section");
					} else if (args[0].equalsIgnoreCase("stop")) {
						if (Utils.isInCreation(player)) {
							Utils.endedCreation(player, false);
							player.sendMessage(ChatColor.RED + "Game creation stopped");
						} else {
							player.sendMessage(ChatColor.RED + "You are not creating a game");
						}
					} else if (args[0].equalsIgnoreCase("next")) {
						if (Utils.getGameFactory(player).getType().isTeam()) {
							if (Utils.creatingWorld.get(player.getUniqueId()) < 6) {
								gameSetupTeamPVP(Utils.creatingWorld.get(player.getUniqueId()), player, (args.length > 1 ? args[1] : null));
							} else {
								if (Utils.getGameFactory(player).getType().equals(GameTypes.CONTROLPOINT)) {
									gameSetupControlPoint(Utils.creatingWorld.get(player.getUniqueId()), player, (args.length > 1 ? args[1] : null));
								} else {
									gameSetupTeamPVP(Utils.creatingWorld.get(player.getUniqueId()), player, (args.length > 1 ? args[1] : null));
								}
							}
						} else {
							if (Utils.creatingWorld.get(player.getUniqueId()) < 5) {
								genGameSetup(Utils.creatingWorld.get(player.getUniqueId()), player, (args.length > 1 ? args[1] : null));
							} else {
								if (Utils.getGameFactory(player).getType().equals(GameTypes.COOP_ZOMBIES)) {
									gameSetupCoopZombies(Utils.creatingWorld.get(player.getUniqueId()), player, (args.length > 1 ? args[1] : null));
								} else {
									genGameSetup(Utils.creatingWorld.get(player.getUniqueId()), player, (args.length > 1 ? args[1] : null));
								}
							}
						}
					} else if (args[0].equalsIgnoreCase("another")) {
						if (Utils.getGameFactory(player).getType().isTeam()) {
							if (Utils.creatingWorld.get(player.getUniqueId()) < 6) {
								gameSetupTeamPVPAnother(Utils.creatingWorld.get(player.getUniqueId()), player);
							} else {
								if (Utils.getGameFactory(player).getType().equals(GameTypes.CONTROLPOINT)) {
									gameSetupControlPointAnother(Utils.creatingWorld.get(player.getUniqueId()), player);
								}
							}
						} else {
							if (Utils.creatingWorld.get(player.getUniqueId()) < 5) {
								genGameSetupAnother(Utils.creatingWorld.get(player.getUniqueId()), player);
							} else {
								if (Utils.getGameFactory(player).getType().equals(GameTypes.COOP_ZOMBIES)) {
									gameSetupCoopZombiesAnother(Utils.creatingWorld.get(player.getUniqueId()), player);
								}
							}
						}
					} else if (args[0].equalsIgnoreCase("getHeads")) {
						if (PlayerXPHandler.getHeads(player.getUniqueId()).isEmpty()) {
							player.sendMessage(ChatColor.RED + "You don't have any heads to claim!");
						} else {
							for (String head : PlayerXPHandler.getHeads(player.getUniqueId())) {
								player.getInventory().addItem(setSkin(new ItemStack(Material.SKULL_ITEM, 1, (byte) 3), head));
							}
						}
						PlayerXPHandler.clearHeads(player.getUniqueId());
					} else if (args[0].equalsIgnoreCase("createClass")) {
						if (args.length < 3) {
							player.sendMessage(ChatColor.RED + "Not enough arguments. Syntax: /upvp createClass <name> <required level>");
							return false;
						}

						new PlayerClass(null, Integer.valueOf(args[2]), args[1], new ClassAttributes(0, 1.0, 0, 1.0, 0, 1.0, 0, 1, false, false, 0, 0.0, false, 1.0, 1.0, 1.0), true);
						player.sendMessage(ChatColor.GREEN + "Good. Now type a description of this class into the chat and hit enter. Also when you hit enter, have the classes armor equipped and the inventory that the class will receive.");
						Utils.setMakingClass(player.getUniqueId(), true, args[1]);
						return true;
					} else if (args[0].equalsIgnoreCase("setAttribute")) {
						if (UltraPVP.classesEnabled) {
							if (args.length < 4) {
								player.sendMessage(ChatColor.RED + "Not enough arguments. Use like this: /upvp setAttribute <class> <attribute> <value>");
								return false;
							}

							if (PlayerXPHandler.getClasses().get(args[1]) != null) {
								for (AttributeType t : AttributeType.values()) {
									if (t.getName().equalsIgnoreCase(args[2])) {
										if ((t.equals(AttributeType.HAS_AURA) || t.equals(AttributeType.HAS_CHANGE)))
										{
											if(!args[3].equalsIgnoreCase("yes") && !args[3].equalsIgnoreCase("no")) {
											
											player.sendMessage(ChatColor.RED + "Invalid data type. That requires a yes or no");
											return false;
										}
										}else if (Double.valueOf(args[3]) == null) {
											player.sendMessage(ChatColor.RED + "Invalid data type. That requires a number");
											return false;
										}

										if (t.equals(AttributeType.BOW_DAMAGE_BONUS)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageBowBoost(Integer.valueOf(args[3]));
										}
										if (t.equals(AttributeType.BOW_DAMAGE_MULTIPLIER)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageBowMult(Double.valueOf(args[3]));
										}
										if (t.equals(AttributeType.SWORD_DAMAGE_BONUS)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageSwordBoost(Integer.valueOf(args[3]));
										}
										if (t.equals(AttributeType.SWORD_DAMAGE_MULTIPLIER)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageSwordMult(Double.valueOf(args[3]));
										}
										if (t.equals(AttributeType.DAMAGE_BONUS)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageBoost(Integer.valueOf(args[3]));
										}
										if (t.equals(AttributeType.DAMAGE_MULTIPLIER)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageMult(Double.valueOf(args[3]));
										}
										if (t.equals(AttributeType.JUMP_BOOST)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setJumpBoost(Integer.valueOf(args[3]));
										}
										if (t.equals(AttributeType.DAMAGE_TAKE_BOW_MULT)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageTakeBowMult(Double.valueOf(args[3]));
										}
										if (t.equals(AttributeType.DAMAGE_TAKE_SWORD_MULT)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageTakeSwordMult(Double.valueOf(args[3]));
										}
										if (t.equals(AttributeType.DAMAGE_TAKE_MULT)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setDamageTakeMult(Double.valueOf(args[3]));
										}
										if (t.equals(AttributeType.HAS_AURA)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setAura(args[3].equalsIgnoreCase("yes"));
										}
										if (t.equals(AttributeType.HAS_CHANGE)) {
											PlayerXPHandler.getClasses().get(args[1]).getAttributes().setChangeCostume(args[3].equalsIgnoreCase("yes"));
										}
										PlayerXPHandler.getClasses().get(args[1]).save();
										player.sendMessage(ChatColor.GREEN + "Attribute successfully changed!");

										return true;
									}
								}
								player.sendMessage(ChatColor.RED + "Invalid attribute. For a list of valid attributes, do /upvp attributes.");
								return false;
							} else {
								player.sendMessage(ChatColor.RED + "Invalid class. Classes are CaSe sEnsiTive");
								return false;
							}
						} else {
							player.sendMessage(ChatColor.RED + "Classes are disabled on this server.");
							return false;
						}
					} else if (args[0].equalsIgnoreCase("attributes")) {
						if (UltraPVP.classesEnabled) {
							for (AttributeType t : AttributeType.values()) {
								player.sendMessage(ChatColor.GREEN + t.getName() + ChatColor.WHITE + " - " + t.description());
							}
							return true;
						} else {
							player.sendMessage(ChatColor.RED + "Classes are disabled on this server");
							return false;
						}
					} else if (args[0].equalsIgnoreCase("gameTypes")) {
						for (GameTypes t : GameTypes.values()) {
							player.sendMessage(ChatColor.GREEN + t.getName());
						}
					} else if (args[0].equalsIgnoreCase("rotate")) {
						if (Utils.getPlayerMap().containsKey(player)) {
							if (Utils.getGameByPlayer(player).getSpectators().contains(player)) {
								Utils.getGameByPlayer(player).teleportSpectator(player);
							} else {
								player.sendMessage(ChatColor.RED + "You are not spectating a game");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not spectating a game");
						}
					} else if (args[0].equalsIgnoreCase("setGear")) {
						if (args.length < 2) {
							player.sendMessage(ChatColor.RED + "Not enough arguments. Syntax: /upvp setGear <className>");
						} else if (PlayerXPHandler.getClasses().get(args[1]) != null) {
							PlayerGearHandler.setClassGear(args[1], player.getUniqueId());
						} else {
							player.sendMessage(ChatColor.RED + "Unrecognized class. Classes are CaSeSeNsItIve");
						}
					} else {
						player.sendMessage(ChatColor.GOLD + "Unknown UltraPVP command. Do /upvp help <command> for help or just /upvp help for general help.");
						return false;
					}
				} else {
					player.sendMessage(ChatColor.GRAY + "UltraPVP v." + plugin.getDescription().getVersion());
					if (plugin.getDescription().getVersion().contains("U") || plugin.getDescription().getVersion().contains("N")) {
						player.sendMessage(ChatColor.RED + "This is a public Unstable build or Nightly build. Unstables generally have no blocker bugs, but can still have many issues. Nightlies should not under any circumstance be trusted and are prone to having absolutely nothing working and possibly setting your computer on fire.");
					}
				}
			}
		}

		return false;
	}

	private static ItemStack setSkin(ItemStack item, String nick) {
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		meta.setOwner(nick);
		item.setItemMeta(meta);
		return item;
	}

	// please redo this fucked up wizard before my head explodes
	private void gameSetupControlPoint(int step, Player player, String arg) {

		switch (step) {
		case 6: {
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			Utils.getGameFactory(player).stepWinkills(Integer.valueOf(arg));
			player.sendMessage(ChatColor.RED + "Very well done. You're getting close now. Please go stand on a red control point and type /upvp another if there are more red control points or /upvp next if that is the only one.");

			return;
		}
		case 7: {
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			Utils.getGameFactory(player).stepRedControlPoint(plugin.getServer().getWorld(player.getWorld().getName()).getBlockAt(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY() - 1, player.getLocation().getZ())));
			player.sendMessage(ChatColor.DARK_AQUA + "Okay. Remember what you did for the red control points? do the same for the blue Control points. Type /upvp another while standing on top of each until you come to the last one. Then type /upvp next while standing on top of it.");

			return;
		}
		case 8: {
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			Utils.getGameFactory(player).stepBlueControlPoint(plugin.getServer().getWorld(player.getWorld().getName()).getBlockAt(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY() - 1, player.getLocation().getZ())));
			player.sendMessage("LAST STEP: Okay, this is your final cold feet moment. Are you sure you want to create this game? If so, type /upvp next. If not, type /upvp stop. " + ChatColor.BOLD + "**WARNING** TYPING /UPVP STOP WILL DESTROY ALL PROGRESS IN MAKING THIS GAME AND IT WILL BE UNRETRIEVABLE!");

			return;
		}
		case 9: {
			player.sendMessage(ChatColor.GREEN + "CONGRATULATIONS! You successfully created a new game with the name of: " + Utils.endedCreation(player, true).getName() + " Make a sign with [UPVP] on the first line and that name on the second to be able to join the new game!");
			return;
		}
		}
	}

	private void gameSetupControlPointAnother(int step, Player player) {

		switch (step) {

		case 7: {
			Utils.getGameFactory(player).stepRedControlPoint(plugin.getServer().getWorld(player.getWorld().getName()).getBlockAt(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY() - 1, player.getLocation().getZ())));
			player.sendMessage(ChatColor.RED + "Nicely done. Now repeat this process for all the red control points and type /upvp next while standing on top of the last one.");
			return;
		}
		case 8: {
			Utils.getGameFactory(player).stepBlueControlPoint(plugin.getServer().getWorld(player.getWorld().getName()).getBlockAt(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY() - 1, player.getLocation().getZ())));
			player.sendMessage(ChatColor.BLUE + "Nicely done. Now repeat this process for all the blue control points and type /upvp next while standing on top of the last one.");
			return;
		}
		}
	}

	@SuppressWarnings("deprecation")
	private static void genGameSetup(int step, Player player, String arg) {
		switch (step) {
		case 1: {
			Utils.getGameFactory(player).stepSpawnLocations(player.getLocation());
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);

			return;
		}
		case 2: {
			// store final chest as a new chest in the game and give
			// instructions to type the number of seconds for spawn protection next
			Block b = player.getTargetBlock(null, 10);
			if (b.getType().equals(Material.CHEST)) {
				Utils.getGameFactory(player).stepAddChest(b);

				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				player.sendMessage(ChatColor.GRAY + "Well done. Now type /upvp next <number_of_Seconds_to_Be_invincible_after_respawning> to continue");
			} else {
				player.sendMessage(ChatColor.RED + "That is not a chest");
			}
			return;
		}
		case 3: {
			// take player input and set the invincible ticks appropriately
			// according to it

			Utils.getGameFactory(player).stepSecondsInvincible(Integer.valueOf(arg));
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			player.sendMessage(ChatColor.DARK_AQUA + "Ok. Now go stand in the first spectator spot. Keep in mind that spectators cannot move and you should give them spots where they can have a nice show. Type /upvp another if there are more or /upvp next if it is the only one.");

			return;
		}
		case 4: {
			// store last spectator location and ask for the amount of kills to
			// win the game
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);

			Utils.getGameFactory(player).stepSpectators(player.getLocation());
			player.sendMessage(ChatColor.GREEN + "Now type /upvp next <number_of_kills_to_win_the_game> ");
			return;
		}
		case 5: {
			if (arg == null) {
				player.sendMessage(ChatColor.RED + "not enough arguments.");
			} else {
				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				Utils.getGameFactory(player).stepWinkills(Integer.valueOf(arg));
				player.sendMessage("LAST STEP: Okay, this is your final cold feet moment. Are you sure you want to create this game? If so, type /upvp next. If not, type /upvp stop. " + ChatColor.BOLD + "**WARNING** TYPING /UPVP STOP WILL DESTROY ALL PROGRESS IN MAKING THIS GAME AND IT WILL BE UNRETRIEVABLE!");
			}
			return;
		}
		case 6: {
			player.sendMessage(ChatColor.GREEN + "CONGRATULATIONS! You successfully created a new game with the name of: " + Utils.endedCreation(player, true).getName() + " Make a sign with [UPVP] on the first line and that name on the second to be able to join the new game!");
			return;
		}
		}
	}

	@SuppressWarnings("deprecation")
	private static void genGameSetupAnother(int step, Player player) {
		switch (step) {
		case 1: {
			Utils.getGameFactory(player).stepSpawnLocations(player.getLocation());
			return;
		}
		case 2: {
			// store chest looked at to new chests
			Block b = player.getTargetBlock(null, 10);
			if (b.getType().equals(Material.CHEST)) {
				Utils.getGameFactory(player).stepAddChest(b);

				player.sendMessage(ChatColor.YELLOW + "Well done. Repeat this process for all the store chests then type /upvp next while looking at the last one.");
			} else {
				player.sendMessage(ChatColor.RED + "That is not a chest");
			}
			return;
		}
		case 4: {
			Utils.getGameFactory(player).stepSpectators(player.getLocation());
			player.sendMessage(ChatColor.AQUA + "Good. Repeat this process until you stand in the last spectator location, then type /upvp next");
			return;
		}
		}
	}

	@SuppressWarnings("deprecation")
	private static void gameSetupCoopZombies(int step, Player player, String arg) {
		switch (step) {
		case 5: {
			if (arg == null) {
				player.sendMessage(ChatColor.RED + "Not enough arguments.");
			} else {
				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				Utils.getGameFactory(player).stepWinkills(Integer.valueOf(arg));
				player.sendMessage(ChatColor.RED + "Well done. Now go stand in the first spot for zombies to spawn and type /upvp another if there are others or type /upvp next if it is the only spawn location for zombies.");
			}
			return;
		}
		case 6: {
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			Utils.getGameFactory(player).stepZombie(player.getLocation());
			player.sendMessage(ChatColor.GREEN + "Well done. Now go look at the medical supply chest and type /upvp next");
			return;
		}
		case 7: {
			Block b = player.getTargetBlock(null, 10);
			if (b.getType().equals(Material.CHEST)) {
				Chest chest = (Chest) b.getState();
				Utils.getGameFactory(player).stepZombieMedChest(chest);

				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				player.sendMessage("LAST STEP: Okay, this is your final cold feet moment. Are you sure you want to create this game? If so, type /upvp next. If not, type /upvp stop. " + ChatColor.BOLD + "**WARNING** TYPING /UPVP STOP WILL DESTROY ALL PROGRESS IN MAKING THIS GAME AND IT WILL BE UNRETRIEVABLE!");
			} else {
				player.sendMessage(ChatColor.RED + "That is not a chest");
			}
			return;
		}
		case 8: {
			player.sendMessage(ChatColor.GREEN + "CONGRATULATIONS! You successfully created a new game with the name of: " + Utils.endedCreation(player, true).getName() + " Make a sign with [UPVP] on the first line and that name on the second to be able to join the new game!");
			return;
		}
		}
	}

	private static void gameSetupCoopZombiesAnother(int step, Player player) {
		switch (step) {
		case 6: {
			Utils.getGameFactory(player).stepZombie(player.getLocation());
			player.sendMessage(ChatColor.BLUE + "Good. Continue this process until you are standing in the final zombie spawn location. Then type /upvp next");
			return;
		}
		}
	}

	@SuppressWarnings("deprecation")
	private static void gameSetupTeamPVP(int step, Player player, String arg) {
		switch (step) {
		case 1: {
			// store players current location to red spawn location tell them to
			// proceed to blue spawn location and hit next
			Utils.getGameFactory(player).stepRedLocation(player.getLocation());
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			player.sendMessage(ChatColor.GOLD + "Good. Now go stand in the blue base where you want blue players to spawn. Then type /upvp next");
			return;
		}
		case 2: {
			// store players current location to blue spawn location and tell
			// them to proceed to store chests to the game
			Utils.getGameFactory(player).stepBlueLocation(player.getLocation());
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
			player.sendMessage(ChatColor.BLUE + "Great. Now go look at the first chest to be used as a store and type /upvp another if there are more or type /upvp next if it is the only store chest");
			return;
		}
		case 3: {
			// store final chest as a new chest in the game and give
			// instructions to type the number of seconds for spawn protection next
			Block b = player.getTargetBlock(null, 10);
			if (b.getType().equals(Material.CHEST)) {
				Utils.getGameFactory(player).stepAddChest(b);

				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				player.sendMessage(ChatColor.GRAY + "Well done. Now type /upvp next <number_of_seconds_to_be_invincible_after_respawning> to continue");
			} else {
				player.sendMessage(ChatColor.RED + "That is not a chest");
			}
			return;
		}
		case 4: {
			// take player input and set the invincible ticks appropriately
			// according to it
			if (arg == null) {
				player.sendMessage(ChatColor.RED + "Not enough arguments.");
			} else {
				Utils.getGameFactory(player).stepSecondsInvincible(Integer.valueOf(arg));
				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				player.sendMessage(ChatColor.DARK_AQUA + "Ok. Now go stand in the first spectator spot. Keep in mind that spectators cannot move and you should give them spots where they can have a nice show. Type /upvp another if there are more or /upvp next if it is the only one.");
			}
			return;

		}
		case 5: {
			// store last spectator location and ask for the amount of kills to
			// win the game
			Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);

			Utils.getGameFactory(player).stepSpectators(player.getLocation());
			player.sendMessage(ChatColor.GREEN + "Now type /upvp next <number_of_kills_to_win_the_game> ");
			return;
		}
		case 6: {
			if (arg == null) {
				player.sendMessage(ChatColor.RED + "not enough arguments.");
			} else {
				Utils.creatingWorld.put(player.getUniqueId(), Utils.creatingWorld.get(player.getUniqueId()) + 1);
				Utils.getGameFactory(player).stepWinkills(Integer.valueOf(arg));
				player.sendMessage("LAST STEP: Okay, this is your final cold feet moment. Are you sure you want to create this game? If so, type /upvp next. If not, type /upvp stop. " + ChatColor.BOLD + "**WARNING** TYPING /UPVP STOP WILL DESTROY ALL PROGRESS IN MAKING THIS GAME AND IT WILL BE UNRETRIEVABLE!");
			}
			return;
		}
		case 7: {
			player.sendMessage(ChatColor.GREEN + "CONGRATULATIONS! You successfully created a new game with the name of: " + Utils.endedCreation(player, true).getName() + " Make a sign with [UPVP] on the first line and that name on the second to be able to join the new game!");
			return;
		}
		}
	}

	@SuppressWarnings("deprecation")
	private static void gameSetupTeamPVPAnother(int step, Player player) {
		switch (step) {
		case 3: {
			// store chest looked at to new chests
			Block b = player.getTargetBlock(null, 10);
			if (b.getType().equals(Material.CHEST)) {
				Utils.getGameFactory(player).stepAddChest(b);

				player.sendMessage(ChatColor.YELLOW + "Well done. Repeat this process for all the store chests then type /upvp next while looking at the last one.");
			} else {
				player.sendMessage(ChatColor.RED + "That is not a chest");
			}
			return;
		}
		case 5: {
			Utils.getGameFactory(player).stepSpectators(player.getLocation());
			player.sendMessage(ChatColor.AQUA + "Good. Repeat this process until you stand in the last spectator location, then type /upvp next");
			return;
		}
		}
	}
}

/* extra code:
if(args[0].equalsIgnoreCase("test-reset")) {
	if(player.isOp()) {
		if(args.length > 1) {
			Utils.getGameMap().get(args[1]).teamWonWithoutPlayer();
		} else {
			Utils.getGameMap().get("DayZ_1").teamWonWithoutPlayer();
		}
	}
}
else
*/